<?php

class GeneratorsCommand extends BaseCommand
{
    // Please to use this command , run every 15 mint

    private $start_time;

    private $end_time;

    private $per_post_day;

    private $direct_push_start;

    private $direct_push_end;

    private $is_custom_settings;

    private $is_direct_push;

    private $scheduled_counter = 0;

    private $gap_time = 0;

    private $PlatFrom;

    private $rule;

    private $template;

    private $main_hash_tag;

    private $hash_tag;

    private $trim_hash;

    private $category;

    private $sub_category;

    private $settings;

    private $debug = false;

    private $courts = false;

    private $array_matches_type = array('graph','pictures','video','videos','graphs');

    private $image_copyright = array(' AP ', ' AFP ', ' Reuters ', ' Getty Images ', ' EPA ', ' Action Images ', ' WAM ');

    private $image_instagram_scheduled = false;

    private $general_image = null;

    private $reason = 'time';

    private $reason_title = '';

    public function run($args)
    {
        system("clear");
        Yii::import('application.modules.features.models.*');

        $this->TimeZone();

        $this->template = PostTemplate::model()->findAll('deleted = 0 ORDER BY RAND()');

        if (empty($this->template)) {

            if ($this->debug)
                echo '[App] : Please insert template ' . PHP_EOL;

            exit();
        }

        $this->rule = Yii::app()->params['rules'];

        $d = Hashtag::model()->findAll('deleted = 0');

        $this->trim_hash = array();

        $this->hash_tag = array();

        foreach ($d as $index => $item) {
            $this->trim_hash[$index] = " #" . str_ireplace(" ", "_", trim($item->title)) . ' ';
            $this->hash_tag[$index] = ' ' . trim($item->title) . ' ';
        }


        $this->main_hash_tag = Yii::app()->params['HashTagRoot'];

        $this->category = Category::model()->findAllByAttributes(array('deleted' => 0, 'active' => 1),array('order'=>'order_by ASC'));


        $this->sub_category = SubCategories::model()->findAllByAttributes(array('deleted' => 0, 'active' => 1));

        $this->settings = Settings::model()->findByPk(1);

        $this->PlatFrom = Platform::model()->findAllByAttributes(array('deleted' => 0));

        $this->per_post_day = $this->settings->how_many_posts;

        $this->start_time = $this->settings->start_date_time;

        $this->end_time = $this->settings->end_date_time;

        $this->gap_time = $this->settings->gap_time;

        $this->direct_push_start = $this->settings->direct_push_start;

        $this->direct_push_end = $this->settings->direct_push_end;

        $this->is_custom_settings = false;

        $this->is_direct_push = $this->settings->direct_push;


        $criteria = New CDbCriteria();

        $criteria->compare('is_scheduled', 1, true);

        $criteria->addCondition('parent_id IS NULL');

        $day = 0;

        $today = date('Y-m-d', strtotime(date('Y-m-d') . ' + ' . $day . ' day'));

        $start = date("Y-m-d H:i:00", strtotime($today . ' ' . $this->start_time));

        $end = date("Y-m-d H:i:00", strtotime($today . ' ' . $this->end_time));

        if ($end < $start)
            $criteria->addBetweenCondition('schedule_date', date('Y-m-d'), date('Y-m-d', strtotime(date('Y-m-d') . ' 1 day')), 'AND');
        else
            $criteria->compare('schedule_date', date('Y-m-d'), true);

        $this->scheduled_counter = PostQueue::model()->count($criteria);

        /** add cron*/

        if (isset($args[1]) and $args[1] == true)
            $this->debug = true;


        if (isset($args[0])) {
            if ($args[0] == 'category')
                $this->rssFeedCategory();
            elseif ($args[0] == 'sub_category')
                $this->rssFeedSubCategory();
            elseif ($args[0] == 'check_news')
                $this->check_and_update_news_url($this->debug);
            else
                exit('[Generator] : Please add category or sub_category after command : {[ php yiic generators category ]} or {[ php yiic generators sub_category ]} ' . PHP_EOL);

        } else {
            exit('[Generator] : Please add category or sub_category after command : {[ php yiic generators category ]} or {[ php yiic generators sub_category ]} ' . PHP_EOL);
        }

    }

    public function clearTag($string)
    {

        $text = str_ireplace('# #', '#', $string);
        $text = str_ireplace('##', '#', $text);
        $text = str_ireplace('&#8220;', '', $text);
        $text = str_ireplace('&#8221;', '', $text);
        $text = str_ireplace('&#8230;', '', $text);
        $text = str_ireplace('&#x2014;', '', $text);
        $text = str_ireplace('#8211;', '', $text);
        $text = str_ireplace('&#8211;', '', $text);
        $text = str_ireplace('&nbsp;', '', $text);
        $text = str_ireplace('&#160;', '', $text);
        $text = str_ireplace('&#xa0;', '', $text);
        $text = str_ireplace('. .', '', $text);
        $text = str_ireplace('&#xf2;,', '', $text);
        $text = str_ireplace('&#8211;', '', $text);
        $text = str_ireplace('2&#xb0;', '', $text);
        return str_ireplace('&ndash;', '', $text);
    }

    private function rssFeedCategory()
    {

        if (!is_array($this->category))
            exit('[Generator] : Category empty Please check  ' . PHP_EOL);


        foreach ($this->category as $sub) {

            $url_data = null;
            if (!empty($sub->url_rss) && $sub->type == 'rss') {
                if(@file_get_contents($sub->url_rss))
                $url_data = file_get_contents($sub->url_rss);

                if (empty(trim($url_data)))
                    continue;

                $xml = @simplexml_load_string($url_data);

                if ($xml)

                    if (isset($xml->channel->item)) {

                        if ($this->debug)
                            echo '[Generator] : Now read from category ' . $sub->url_rss . PHP_EOL;

                        foreach ($xml->channel->item as $item) {



                            $data['sub_category_id'] = $sub->id;

                            $data['category_id'] = $sub->id;

                            $data['creator'] = null;

                            $data['column'] = null;

                            $data['num_read'] = null;

                            $data['title'] = null;

                            $data['link'] = null;

                            $data['link_md5'] = null;

                            $data['description'] = null;

                            $data['body_description'] = null;

                            $data['shorten_url'] = null;

                            $data['publishing_date'] = null;

                            $published = null;

                            $e_content = $item->children("content", true);

                            if (isset($e_content->encoded))
                                $e_encoded = (string)$e_content->encoded;

                            if (isset($item->pubDate))
                                $pub_date = (string)$item->pubDate;


                            if (isset($pub_date)) {
                                $new_pub = explode(' ', $pub_date);
                                array_pop($new_pub);
                                $pub_date = implode(' ', $new_pub);
                                $published = date('Y-m-d', strtotime($pub_date));
                            }

                            /*if (true) {*/
                            if ((isset($published) && $published == date('Y-m-d')) or true) {
                                if (isset($item->link))
                                    $data['link'] = trim((string)$item->link);

                                if (isset($item->link))
                                    $data['link_md5'] = md5((string)$item->link);

                                $news = News::model()->findByAttributes(array('link_md5' => $data['link_md5']));

                                if (empty($news)) {

                                    $details = $this->get_details((string)$item->link, $sub->id);
                                    if(empty($details)){
                                        if($this->debug){
                                            echo 'Link cannot be oppend'.PHP_EOL;
                                        }
                                        continue;
                                    }

                                    if (isset($details['creator']))
                                        $data['creator'] = $details['creator'];

                                    if (empty(trim($details['title'])))
                                        $data['title'] = strip_tags((string)$item->title);
                                    else{
                                        $data['title'] = $details['title'];
                                    }
                                    if (empty(trim($details['description'])))
                                        $data['description'] = strip_tags((string)$item->description);
                                    else{
                                        $data['description'] = $details['description'];
                                    }

                                    if (isset($e_encoded))
                                        $data['body_description'] = strip_tags($e_encoded);
                                    else
                                        $data['body_description'] = isset($details['body_description']) ? $details : null;


                                    if (isset($pub_date))
                                        $data['publishing_date'] = date('Y-m-d H:i:00', strtotime(trim($pub_date)));

                                    if (isset($details['link']) and !empty($details['link'])) {
                                        $data['link'] = trim($details['link']);
                                        $data['link_md5'] = md5($details['link']);
                                    }
                                    $data['media'] = $details['media'];


                                    if ($this->debug)
                                        echo '[Generator] : Now add new { news } : ' . $data['link_md5'] . PHP_EOL;

                                    $rechecked_news = News::model()->findByAttributes(array('link_md5' => $data['link_md5']));
                                    if (empty($rechecked_news)) {
                                        $this->AddNews($data);
                                    } else {
                                        if ($this->debug)
                                        {
                                            echo "{Generator} : after rechecked link already exist" . PHP_EOL;
                                            echo '..............................................' . PHP_EOL;
                                            echo '..............................................' . PHP_EOL;
                                            echo '..............................................' . PHP_EOL;
                                            echo '..............................................' . PHP_EOL;
                                            echo '..............................................' . PHP_EOL;
                                            echo '..............................................' . PHP_EOL;
                                            echo '..............................................' . PHP_EOL;
                                            echo '..............................................' . PHP_EOL;
                                        }
                                    }
                                } else
                                    if ($this->debug)
                                        echo '[Generator] : This { news } generate before at { ' . $news->created_at . ' } : ' . $data['link_md5'] . PHP_EOL;
                            } else
                                if ($this->debug)
                                    echo '[Generator] : old date : ' . $published . PHP_EOL;

                        }
                    } else  if($this->debug)echo '[Generator] : chanel not set ' . $sub->url_rss . PHP_EOL;
                if ($this->debug)
                    echo '[Generator] : Error load date: ' . $sub->url_rss . PHP_EOL;
            } elseif (!empty($sub->url) && $sub->type == 'dom') {

                $html = new SimpleHTMLDOM();

                $html_url = $html->file_get_html($sub->url);
                if (isset($html_url)) {
                    $categories_predication = CategoryLevel::model()->findAllByAttributes(array('source_id' => $sub->id));
                    foreach ($categories_predication as $category_predict) {
                        $find = $category_predict->predication;
                        if (strpos($find, 'div[class=containermid-last] ') !== false) {
                            $words = explode(' ', $find);
                            array_shift($words);
                            $find = implode(' ', $words);
                            $list = $html_url->find('div[class=containermid-last] ', 0)->find($find);
                        } else {
                            $list = $html_url->find($find);
                        }
                        if (isset($list)) {
                            foreach ($list as $item) {

                                $url = null;
                                $create = null;
                                $date = null;
                                if (isset($item->href) && !empty($item->href)) {
                                    if (strpos($item->href, 'http') === 0) {
                                        $url = $item->href;
                                    } else
                                        $url = Yii::app()->params['feedUrl'] . $item->href;

                                    $news = News::model()->findByAttributes(array('link_md5' => md5($url)));

                                    if (empty($news)) {
                                        $data['sub_category_id'] = null;

                                        $data['category_id'] = $sub->id;

                                        $data['creator'] = null;

                                        $data['column'] = null;

                                        $data['num_read'] = null;

                                        $data['title'] = null;

                                        $data['link'] = $url;

                                        $data['link_md5'] = md5($url);

                                        $data['description'] = null;

                                        $data['body_description'] = null;

                                        $data['shorten_url'] = null;

                                        $data['publishing_date'] = $date;

                                        if (!empty($data['link'])) {

                                            $details = $this->get_details($url, $sub->id);
                                            if(empty($details)){
                                                if($this->debug){
                                                    echo 'Link cannot be oppend'.PHP_EOL;
                                                }
                                                continue;
                                            }

                                            if (isset($details['title'])) {

                                                $data['title'] = $details['title'];
                                            }
                                            if (isset($details['publishing_date'])) {
                                                $dates = str_replace("/", "-", $details['publishing_date']);

                                                $date = date('Y-m-d', strtotime($dates));

                                                $data['publishing_date'] = $date;
                                            }
                                            if (isset($details['description'])) {

                                                $data['description'] = $details['description'];
                                            }
                                            if (isset($details['body_description'])) {
                                                $data['body_description'] = $details['body_description'];
                                            }
                                            if (isset($details['creator'])) {
                                                $data['creator'] = $details['creator'];
                                            }
                                            if (isset($details['tags'])) {
                                                $data['tags'] = $details['tags'];
                                            }
                                            $data['media'] = $details['media'];


                                            if (( $data['publishing_date'] == date('Y-m-d') ) or true ) {
                                                $this->AddNews($data);
                                            } else {
                                                if($this->debug)
                                                    echo '[Website] : This news is old date : ' . $data['publishing_date'] . ' Link is : ' . $url . PHP_EOL;
                                                break;
                                            }
                                        } else {
                                            if($this->debug) echo '[link] : Empty ' . $sub->url;

                                        }
                                    } else {
                                        if($this->debug)   echo '[Link] is already exist';
                                    }


                                } else
                                    if($this->debug)  echo '[Website] : Url empty or does not exist in the current sub category id = ' . $sub->id . PHP_EOL;
                            }
                        }
                    }
                } else
                    if($this->debug)   echo '[Sub link] : There is no data in this sub category ' . $sub->title . PHP_EOL;


            } else {
                if($this->debug)  echo "file name empty";
            }
        }

    }

    private function custom_settings($times, $platform_id, $cat_id){

        $this->is_custom_settings = true;

        $condition = new CDbCriteria();

        $condition->order = 'id Desc';

        $condition->limit = 1;

         $condition->condition = 'is_scheduled = 1 and settings="custom" and platform_id = ' . $platform_id . ' and catgory_id = ' . $cat_id;
        // $condition->condition = 'is_scheduled = 1 and settings="custom" and platform_id = '.$platform_id;

        $check = PostQueue::model()->find($condition);

        $final_date = strtotime(date('Y-m-d H:i:00'));

        $obj = null;

        foreach ($times as $timeItem) {
            $timeItem = (object)$timeItem;

            $obj = $timeItem->obj_id;

            if ($timeItem->is_direct_push) {
                $direct_start = strtotime(date('Y-m-d') . ' ' . $timeItem->direct_push_start_time);
                $direct_end = strtotime(date('Y-m-d') . ' ' . $timeItem->direct_push_end_time);

                if ($direct_end < $direct_start) {
                    $extra_day = 1;
                    $direct_end = strtotime(date('Y-m-d') . ' ' . $timeItem->direct_push_end_time . ' + ' . $extra_day . ' day');
                }
                $now = strtotime(date('Y-m-d H:i:00'));
                if (($now >= $direct_start) && ($now <= $direct_end)) {
                    $final_date = strtotime(date('Y-m-d H:i:00') . ' - 1 minute');
                    $is_scheduled = 1;
                    $post_date = date('Y-m-d H:i:00', $final_date);

                    return array($post_date, $is_scheduled, $obj,$check);

                }

            }
        }


        $gap_time = 0;

        $is_scheduled = 0;

        $start = null;

        $end = null;


        foreach ($times as $timeItem) {


            if ($this->debug)
                echo '[Generator] : Start Foreach times line 621 ' . PHP_EOL;

            $timeItem = (object)$timeItem;

            $obj = $timeItem->obj_id;

            $start = strtotime(date('Y-m-d') . ' ' . $timeItem->start_time);

            $end = strtotime(date('Y-m-d') . ' ' . $timeItem->end_time);

            $now = strtotime(date('Y-m-d H:i:00') . ' + ' . $timeItem->gap_time . ' minutes');

            $gap_time = $timeItem->gap_time;

            if ($end < $start) {
                $extra_day = 1;
                $end = strtotime(date('Y-m-d') . ' ' . $timeItem->end_time . ' + ' . $extra_day . ' day');
            }

            if (empty($check)) {

                if($this->debug && $platform_id==2)
                    echo '$check empty'.PHP_EOL;

                if (($now <= $start)) {
                    $final_date = $start;
                    $is_scheduled = 1;
                    echo '$now <= $start'.date('Y-m-d H:i:00',$now) .'----'. date('Y-m-d H:i:00',$start).PHP_EOL;
                    break;
                } elseif ($now <= $end) {
                    $final_date = $now;
                    echo '$now <= $end'.date('Y-m-d H:i:00',$now) .'----'. date('Y-m-d H:i:00',$end).PHP_EOL;
                    $is_scheduled = 1;
                    break;
                }

            } else {
                if($this->debug && $platform_id==2)
                    echo '$check !empty date '.$check->schedule_date.PHP_EOL;

                $date = strtotime($check->schedule_date . ' + ' . $timeItem->gap_time . ' minutes');
                if ($date < $now)
                        $date = $now;

                if (($date <= $start)) {
                    $final_date = $start;
                    $is_scheduled = 1;
                    break;

                } elseif ($date <= $end) {
                    $final_date = $date;
                    $is_scheduled = 1;
                    break;

                }
            }
        }

        $post_date = date('Y-m-d H:i:00', $final_date);

        echo $post_date.'    gap '.$gap_time.PHP_EOL;

        while ($this->check_if_date_exist($post_date, $platform_id))
            $post_date = date('Y-m-d H:i:00', strtotime($post_date . ' + '.$gap_time.' minutes'));


        $temp_date=strtotime($post_date);

        if (($temp_date<$start  ) || ($temp_date> $end))
            $is_scheduled = 0;




        return array($post_date, $is_scheduled, $obj,$check);

    }

    private function check_if_date_exist($date, $platform_id)
    {

        $condition = new CDbCriteria();

        $condition->condition = 'is_scheduled = 1 and schedule_date = "' . $date . '" and platform_id = ' . $platform_id;

        return !empty(PostQueue::model()->count($condition));

    }

    private function date($cat_id, $platform_id,$news)
    {

        $p = null;

        if($this->debug)
            echo PHP_EOL.'[App] : news id is '.$news->id . PHP_EOL.PHP_EOL;

        foreach ($this->PlatFrom as $item) {
            if($item->id == $platform_id){

                $p  =  $item->title;
                if($this->debug)
                    echo PHP_EOL.'[App] : Platform now is '.$item->title . PHP_EOL.PHP_EOL;

            }
        }


        $categoy = false;



        //check category

        $cat = Yii::app()->db->createCommand("
                                      SELECT
                platform_category_settings.obj_id,
                platform_category_settings.platform_id
                FROM platform_category_settings
                INNER JOIN settings_obj
                ON settings_obj.id =platform_category_settings.obj_id
                 WHERE 
                 ( platform_category_settings.category_id = " . $cat_id . ")  
                 and 
                 platform_category_settings.platform_id = " . $platform_id . "
                 and 
                 settings_obj.active = 1
                 ORDER BY category_id desc;
        ")->queryAll();

        if(!empty($cat)){

            $times = null;
            $times = Yii::app()->db->createCommand("
                                      SELECT
                platform_category_settings.obj_id,
                platform_category_settings.platform_id,
                time_settings.start_time,
                time_settings.end_time,
                time_settings.is_direct_push,
                time_settings.direct_push_start_time,
                time_settings.direct_push_end_time,
                time_settings.gap_time
                FROM platform_category_settings
                
                INNER JOIN days_settings
                ON days_settings.platform_category_settings_id =platform_category_settings.id
                
                INNER JOIN day
                ON days_settings.day_id =day.id
                
                INNER JOIN time_settings
                ON time_settings.platform_category_settings_id =platform_category_settings.id
                INNER JOIN settings_obj
                ON settings_obj.id =platform_category_settings.obj_id
                
                 WHERE 
                 ( platform_category_settings.category_id = " . $cat_id . " )  
                 and 
                 platform_category_settings.platform_id = " . $platform_id . "
                 and
                 day.title = DAYNAME(NOW())
                 and 
                 settings_obj.active = 1
                 ORDER BY category_id desc;
        ")->queryAll();


            if (!empty($times)) {

                list($time, $is_scheduled, $obj , $check) = $this->custom_settings($times, $platform_id, $cat_id);

                if (!$this->debug)
                    echo '[App] : time is : '.$time.' scheduled is ' . ( $is_scheduled ? 'True' : 'false') . PHP_EOL;

                if ($this->debug && $p == 'Twitter'){

                    $c = null;
                    if(isset($check->id)){
                        $c  = ' Post check id is '.$check->id.' and settings is '.$check->settings;
                    }
                    echo '[App] : final date '.$time.'  final is scheduled ' .( $is_scheduled ? 'True' : 'false').' Platform is '.$p .' news id is '.$news->id .$c. PHP_EOL;
                }



                return array($time, $is_scheduled, $obj );

            }

            $categoy = true;
        }

        $times = null;

        //end check category


        $where = "( platform_category_settings.category_id = " . $cat_id . " or platform_category_settings.category_id IS NULL )";

        if($categoy)
            $where = "( platform_category_settings.category_id != " . $cat_id . " and platform_category_settings.category_id IS NULL )";


        $times = Yii::app()->db->createCommand("
                                      SELECT
                platform_category_settings.obj_id,
                platform_category_settings.platform_id,
                time_settings.start_time,
                time_settings.end_time,
                time_settings.is_direct_push,
                time_settings.direct_push_start_time,
                time_settings.direct_push_end_time,
                time_settings.gap_time,
                platform_category_settings.category_id
                FROM platform_category_settings
                
                INNER JOIN days_settings
                ON days_settings.platform_category_settings_id =platform_category_settings.id
                
                INNER JOIN day
                ON days_settings.day_id =day.id
                
                INNER JOIN time_settings
                ON time_settings.platform_category_settings_id =platform_category_settings.id
                INNER JOIN settings_obj
                ON settings_obj.id =platform_category_settings.obj_id
                
                 WHERE 
                  ".$where."  
                 and 
                 platform_category_settings.platform_id = " . $platform_id . "
                 and
                 day.title = DAYNAME(NOW())
                 and 
                 settings_obj.active = 1
                 ORDER BY category_id desc;
        ")->queryAll();




        if (!empty($times)) {

            list($time, $is_scheduled, $obj , $check) = $this->custom_settings($times, $platform_id, $cat_id);




            if (!$this->debug)
                echo '[App] : time is : '.$time.' scheduled is ' . ( $is_scheduled ? 'True' : 'false') . PHP_EOL;

            if ($this->debug && $p == 'Twitter'){

                $c = null;
                if(isset($check->id)){
                    $c  = ' Post check id is '.$check->id.' and settings is '.$check->settings;
                }
                echo '[App] : final date '.$time.'  final is scheduled ' .( $is_scheduled ? 'True' : 'false').' Platform is '.$p .' news id is '.$news->id .$c. PHP_EOL;
            }



            return array($time, $is_scheduled, $obj );

        }

        $this->is_custom_settings = false;

        $final_date = null;

        $condition = new CDbCriteria();

        $condition->order = 'id Desc';

        $condition->limit = 1;

        $condition->condition = 'is_scheduled = 1 and settings="general" and platform_id = ' . $platform_id;


        $check = PostQueue::model()->find($condition);
        $is_scheduled = 1;
        $now = strtotime(date('Y-m-d H:i:00') . ' + ' . $this->gap_time . ' minutes');
        $start = strtotime(date('Y-m-d') . ' ' . $this->start_time);
        $end = strtotime(date('Y-m-d') . ' ' . $this->end_time);

        if ($end < $start) {
            $extra_day = 1;
            $end = strtotime(date('Y-m-d') . ' ' . $this->end_time . ' + ' . $extra_day . ' day');
        }



        if ($this->is_direct_push) {

            if ($this->debug)
                echo '[App] : General Settings is direct push ' . $this->is_direct_push? 'True' : 'false' . PHP_EOL;


            $direct_start = strtotime(date('Y-m-d') . ' ' . $this->direct_push_start);
            $direct_end = strtotime(date('Y-m-d') . ' ' . $this->direct_push_end);

            if ($direct_end < $direct_start) {
                $extra_day = 1;
                $direct_end = strtotime(date('Y-m-d') . ' ' . $this->direct_push_end . ' + ' . $extra_day . ' day');
            }
            $now = strtotime(date('Y-m-d H:i:00'));
            if (($now >= $direct_start) && ($now <= $direct_end)) {
                $final_date = strtotime(date('Y-m-d H:i:00') . ' -1 minute');
                $is_scheduled = 1;
            }
        }else{


            if ($this->debug)
                echo '[App] : General Settings ' . $this->is_direct_push? 'True' : 'false' . PHP_EOL;



            if (empty($check)) {
                if (($now <= $start)) {
                    $final_date = $start;

                } elseif ($now <= $end) {
                    $final_date = $now;
                } else {
                    $final_date = $now;
                    $is_scheduled = 0;
                }
            } else {
                $date = strtotime($check->schedule_date . ' + ' . $this->gap_time . ' minutes');
                if ($date < $now) {
                    $date = $now;
                }

                if (($date <= $start)) {
                    $final_date = $start;
                } elseif ($date <= $end) {
                    $final_date = $date;
                } else {
                    $final_date = $date;
                    $is_scheduled = 0;
                }
            }

        }

        $post_date = date('Y-m-d H:i:00', $final_date);


        $times = Yii::app()->db->createCommand("SELECT  platform_category_settings.obj_id FROM platform_category_settings
                INNER JOIN settings_obj ON settings_obj.id =platform_category_settings.obj_id
                 WHERE 
                 ( platform_category_settings.category_id = " . $cat_id . " or platform_category_settings.category_id IS NULL )  
                 and 
                 platform_category_settings.platform_id = " . $platform_id . "
                 and 
                 settings_obj.active = 1
        ")->queryAll();

        if ($this->debug)
            echo '[App] : General Settings Platform id :  ' . $platform_id . PHP_EOL;

        if (!empty($times)){

            $is_scheduled = 0;

            if ($this->debug)
                echo '[App] : general $times $is_scheduled :  ' . $is_scheduled . PHP_EOL;
        }

        if ($this->debug)
            if (!empty($times))
                echo '[App] : general This platform there are custom settings ' . $platform_id . PHP_EOL;


        if ($this->debug)
            if ($is_scheduled)
                echo '[App] : general scheduled is True' . PHP_EOL;
            else
                echo '[App] : general scheduled is False' . PHP_EOL;


        while ($this->check_if_date_exist($post_date, $platform_id))

            $post_date = date('Y-m-d H:i:00', strtotime($post_date . ' + '.$this->gap_time.' minutes'));

        $temp_date=strtotime($post_date);

        if (($temp_date<$start  ) || ($temp_date> $end))
            $is_scheduled = 0;


        if (!$this->debug && $p == 'Twitter') {
            $c = null;
            if(isset($check->id)){
                $c  = ' Post check id is '.$check->id.' and settings is '.$check->settings;
            }

            echo '[App] : final date ' . $post_date . '  final is scheduled ' . ($is_scheduled ? 'True' : 'false') . ' Platform is ' . $p . ' news id is ' . $news->id . $c . PHP_EOL;
        }

        return array($post_date, $is_scheduled, false);

    }

    private function AddNews($data){
        $this->image_instagram_scheduled = false;

        $news = new News();

        $news->id = null;

        $news->link = $data['link'];

        $news->link_md5 = $data['link_md5'];

        $news->category_id = $data['category_id'];

        $news->sub_category_id = null;

        $news->title = $data['title'];

        $news->column = $data['column'];

        $news->description = $data['description'];

        $news->publishing_date = $data['publishing_date'];

        $news->schedule_date = $data['publishing_date'];

        $news->created_at = date('Y-m-d H:i:00');

        $news->creator = $data['creator'];

        $news->shorten_url = $this->GoogleShort($news->link);

        $news->setIsNewRecord(true);

        if ($news->save(true)) {

            $id = null;

            if (isset($data['media']['video']))
                $id = $this->AddMediaNews($news->id, 'video', $data['media']['video']);


            if (isset($data['media']['gallery']))
                $id = $this->AddMediaNews($news->id, 'gallery', $data['media']['gallery']);
            if (isset($data['media']['image']))
                $id = $this->AddMediaNews($news->id, 'image', $data['media']['image']);

            $this->newsGenerator($news);

        }else{
            print_r($news->getErrors());
            die;
        }
    }

    private function AddMediaNews($news_id, $type, $data)
    {


        $id = null;

        $find = true;

        if ($type == 'gallery') {
            foreach ($data as $item) {
                if (isset($item['caption']) && !empty($item['caption']) && $find) {
                    foreach ($this->image_copyright as $query) {
                        if (!empty(stristr($item['caption'], trim($query)))) {
                            $find = false;
                            $this->image_instagram_scheduled = true;
                        }
                    }
                }
                if (empty(MediaNews::model()->findByAttributes(array('media' => $item['src'], 'news_id' => $news_id, 'type' => $type)))) {
                    $media = new MediaNews();
                    $media->type = $type;
                    $media->media = $item['src'];
                    $media->news_id = $news_id;
                    $media->setIsNewRecord(true);
                    $media->save(false);
                    if ($id == null)
                        $id = $media->id;
                }
            }
            return $id;
        }

        if (empty(MediaNews::model()->findByAttributes(array('media' => $data, 'news_id' => $news_id, 'type' => $type)))) {
            $media = new MediaNews();
            $media->type = $type;
            $media->media = $data;
            $media->news_id = $news_id;
            $media->setIsNewRecord(true);
            $media->save(false);
            if ($id == null)
                $id = $media->id;
        }
        return $id;


    }

    protected function newsGenerator($news){



        $parent = null;

        $category = null;
        $valid_twitter = false;

        if (isset($news->category->title))
            $category = '#' . trim(str_ireplace(array('!', '"', ':', '.', '..', '...', ' '), '_', $news->category->title));

        $sub_category = null;

        if (isset($news->subCategory->title))
            $sub_category = '#' . trim(str_ireplace(array('!', '"', ':', '.', '..', '...', ' '), '_', $news->subCategory->title));
        $cdb = new CDbCriteria();

        $cdb->condition = 'news_id = ' . $news->id . ' and type != "video" and type !="gallery"';

        $data_media_original = MediaNews::model()->find($cdb);


        if ($this->settings->enable_download_videos == 1) {
            if ($this->get_video_in_news($news->id)) {


                if ($this->debug)
                    echo '[generate] : now in get video in news ' . PHP_EOL;

                $valid_twitter = true;

                list($data_media, $valid, $path) = $this->get_media_news($news);

                if ($valid) {

                    if ($this->debug)
                        echo '[generate] : now in get media news is valid  : -> ' . $valid . '  in news ' . PHP_EOL;


                    if (!empty($data_media))
                        $data_media_original = $data_media;

                    if ($this->video_size($path)) {


                        if ($this->debug)
                            echo '[generate] : now in video size is valid  : -> ' . $valid . '  in news ' . PHP_EOL;


                        sleep(10);
                        $valid_twitter = false;


                    }
                } else {
                    if ($this->debug)
                        echo '[generate] : now in get media news is valid  : -> ' . $valid . '  in news ' . PHP_EOL;
                }


                if (isset($path)) {

                    if ($this->debug)
                        echo '[generate] : Delete file from server {{ ' . $path . ' }}' . PHP_EOL;
                    $this->delete_file_from_server($path);
                }
            }
        } else {
            if($this->debug)
                echo 'video downloader is disabled';
        }


        if (empty($data_media_original)) {

            $cdb = new CDbCriteria();

            $cdb->condition = 'news_id = ' . $news->id . ' and type != "video"';

            $data_media_original = MediaNews::model()->find($cdb);
        }


        foreach ($this->PlatFrom as $item) {

            $new_shorten = $news->link;

            if (!empty($news->link)) {

                switch ($item->title) {
                    case 'Facebook':
                        $new_shorten .= '?utm_source=facebook&utm_medium=referral&utm_campaign=sortechs';
                        break;
                    case 'Twitter':
                        $new_shorten .= '?utm_source=twitter&utm_medium=referral&utm_campaign=sortechs';
                        break;

                    case 'Instagram':
                        $new_shorten .= '?utm_source=instagram&utm_medium=referral&utm_campaign=sortechs';
                        break;
                }
            }
            if (!empty($new_shorten)) {

                $new_shorten = $this->GoogleShort($new_shorten);
            } elseif (!empty($news->shorten_url)) {
                $new_shorten = $news->shorten_url;
            }

            $media = null;

            if ($item->title == "Twitter" && $valid_twitter)
                $data_media_original = $this->get_media_for_twitter($news);

            if ($item->title == "Instagram") {
                $data_media_original = $this->get_media_for_instagram($news);
            }

            $temp = $this->get_template($item, $news->category_id, isset($data_media_original->type) ? $data_media_original->type : "Image");


            $title = $news->title;

            $description = $news->description;

            if ($item->title != 'Instagram') {
                $title = str_ireplace($this->hash_tag, $this->trim_hash, $news->title);
                $description = str_ireplace($this->hash_tag, $this->trim_hash, $news->description);
            }

            $title = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $title);

            $title = preg_replace('/\n(\s*\n){2,}/', "\n\n", $title);

            $description = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $description);

            $description = preg_replace('/\n(\s*\n){2,}/', "\n\n", $description);

            $shorten_url = $news->shorten_url;

            $full_creator = $news->creator;

            $text = str_ireplace(
                array('[title]', '[description]', '[short_link]', '[author]'),
                array($title, $description, $shorten_url, $full_creator),
                $temp['text']
            );

            if ($item->title == 'Facebook' or $item->title == 'Twitter') {

                $text = str_ireplace('# ', '#', $text);

                $found = true;

                preg_match_all("/#([^\s]+)/", $text, $matches);

                if (isset($matches[0])) {

                    $matches[0] = array_reverse($matches[0]);
                    $count = 0;
                    foreach ($matches[0] as $hashtag) {


                        if (strpos($text, '[section]')) {
                            if ($count >= 1) {
                                $text = str_ireplace($hashtag, str_ireplace('_', ' ', str_ireplace('#', '', $hashtag)), $this->clear_tags($text));
                                break;
                            }
                        } else {
                            if ($count >= 2) {
                                $found = false;
                                $text = str_ireplace($hashtag, str_ireplace('_', ' ', str_ireplace('#', '', $hashtag)), $text);
                            }
                        }
                        $count++;
                    }
                    $found = true;
                    if ($count >= 2)
                        $found = false;
                }


                if ($found)
                    $text = str_ireplace(array('[section]', '[sub_section]'), array($category, $sub_category), $text);
                else
                    $text = str_ireplace(array('[section]', '[sub_section]', '|'), array('', '', '',), $text);

            } elseif ($item->title == 'Instagram') {
                $text = str_ireplace(array('[section]', '[sub_section]', '|'), array('', '', ''), str_ireplace('# ', '#', $text));

            }

            list($time, $is_scheduled, $obj) = $this->date($news->category_id, $item->id,$news);


            if ($this->debug)
                echo '[Generator] : scheduled is '.$is_scheduled.' from date function'. PHP_EOL;

            if ($this->debug)
                echo '[Generator] : Now generate ' . $item->title . ' , Time : ' . $time . PHP_EOL;


            if ($item->title == 'Twitter') {

                $text_twitter = $text;
                if ($temp['type'] == 'Preview')
                    if (!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', $text, $matches))
                        $text = $text . PHP_EOL . $news->shorten_url;


                if ($this->getTweetLength($text_twitter, $temp['type'] == 'Image' ? true : false, $temp['type'] == 'Video' ? true : false) > 141) {
                    $is_scheduled = 0;
                    if($is_scheduled==0){

                        $this->reason = 'other';
                        $this->reason_title = 'Twitter Length';
                    }


                    if ($this->debug)
                        echo '[Generator] : Twitter Length'. PHP_EOL;

                } else {
                    $text = $text_twitter . PHP_EOL;
                    if ($temp['type'] == 'Preview')
                        if (!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', $text, $matches))
                            $text .= PHP_EOL . $news->shorten_url;
                }
            }


            $text = $this->clearTag($text);

            $coun_c = $this->scheduled_counter / 3 ;

            if ($coun_c > $this->per_post_day) {

                if ($this->debug)
                    echo '[Generator] : news today ' . $this->scheduled_counter . '  /3   '.$coun_c.' , { Time }' . $this->per_post_day . PHP_EOL;

                $is_scheduled = 0;

                if($is_scheduled==0){

                    $this->reason = 'other';
                    $this->reason_title = 'post per day more than';
                }
            }

            if ($item->title == 'Instagram') {

                $text = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $text);
                $text = preg_replace('/\n(\s*\n){2,}/', "\n\n", $text);


                if ($this->image_instagram_scheduled) {

                    if ($this->debug)
                        echo '[Generator] : is scheduled after instagram : ' . $is_scheduled . PHP_EOL;

                    $this->image_instagram_scheduled = true;

                }

                if ($media == 'http://www.thenational.ae/styles/images/default_social_share.jpg')
                    $this->image_instagram_scheduled = true;


            if ($this->image_instagram_scheduled)
            {
                if ($this->debug)
                    echo '[Generator] : now in if image instagram '.PHP_EOL;

                $is_scheduled = 0;
                if($is_scheduled==0){

                    $this->reason = 'other';
                    $this->reason_title = 'instagram rule ';
                }
            }


                foreach ($this->array_matches_type as $array_matches){
                    if (preg_match('/\b' . $array_matches . '\b/u', '#' . $text, $matches)) {

                        $is_scheduled=0;
                        if($is_scheduled==0){

                            $this->reason = 'other';
                            $this->reason_title = 'key word ';
                        }
                        if ($this->debug)
                            echo '[Generator] : now in for loop keyword match platform -> instagram '.PHP_EOL;
                        break;
                    }
                }

            }


            if ($this->debug)
                echo '[Generator] : is scheduled  : ' . $item->title . ' ' . $is_scheduled . PHP_EOL;



            if ($this->courts == true) {
                if ($item->title == 'Instagram' || $item->title == 'Facebook') {
                    $is_scheduled = 0;
                    if($is_scheduled==0){

                        $this->reason = 'other';
                        $this->reason_title = 'category- courts';
                    }
                }
            }

            if($is_scheduled){

                $this->scheduled_counter++;


                if ($this->debug)
                    echo '[Generator] : scheduled counter now is : '.$this->scheduled_counter.' total '.$coun_c . PHP_EOL;

            }


            if ($this->debug)
                echo '[Generator] : is scheduled before add   : ' . $item->title . ' ' . $is_scheduled.'      news id is ::  '.$news->id . PHP_EOL;

            if($item->title == 'Instagram'){
                $is_scheduled=0;
                if($is_scheduled==0){
                    $this->reason_title = 'instagram rule';
                    $this->reason = 'other';
                }
            }
            if($item->title == 'Facebook'|| $item->title == 'Instagram') {
                if (isset($data_media_original->media)) {
                    if ('http://www.thenational.ae/styles/images/default_social_share.jpg' == $data_media_original->media) {
                        $is_scheduled = 0;
                        if($is_scheduled==0){
                            $this->reason_title = 'default social share image';
                            $this->reason = 'other';
                        }
                    }
                }
            }
            $PostQueue = new PostQueue();
            $PostQueue->setIsNewRecord(true);
            $PostQueue->id = null;
            $PostQueue->command = false;
            $PostQueue->type = $temp['type'];
            $PostQueue->post = trim($text);
            $PostQueue->schedule_date = date('Y-m-d H:i:00',strtotime($time));
            $PostQueue->catgory_id = $news->category_id;
            $PostQueue->main_category_id = $obj ? $obj : null;
            $PostQueue->link = $new_shorten;
            $PostQueue->is_posted = 0;
            $PostQueue->news_id = $news->id;
            $PostQueue->post_id = null;
            $PostQueue->media_url = isset($data_media_original->media) ? $data_media_original->media : $this->general_image;
            $PostQueue->settings = $this->is_custom_settings ? 'custom' : 'general';
            $PostQueue->is_scheduled = $is_scheduled;
            $PostQueue->platform_id = $item->id;
            $PostQueue->generated = 'auto';
            $PostQueue->errors = $this->reason_title;
            $PostQueue->reason = $this->reason;
            $PostQueue->created_at = date('Y-m-d H:i:s');

            if ($parent == null) {

                $PostQueue->parent_id = null;

                if ($PostQueue->save())
                    $parent = $PostQueue->id;
                else
                    $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/postQueue/' . $PostQueue->id . '">'.Yii::app()->params['domain'] . '/postQueue/' . $PostQueue->id.'</a>');

            } else {
                $PostQueue->parent_id = $parent;
                if (!$PostQueue->save())
                    $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/postQueue/' . $PostQueue->id . '">'.Yii::app()->params['domain'] . '/postQueue/' . $PostQueue->id.'</a>');
            }


            if($this->debug)
            echo PHP_EOL;


        }

        $news->generated = 1;
        $this->reason = 'time';
        if (!$news->update())
            $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/news/' . $news->id . '">'.Yii::app()->params['domain'] . '/postQueue/' . $news->id.'</a>');
        if(!$this->debug)
            echo PHP_EOL.PHP_EOL.PHP_EOL.PHP_EOL;

    }


}