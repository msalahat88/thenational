<?php
/* @var $this CategoryLevelController */
/* @var $model CategoryLevel */

$this->breadcrumbs=array(
	'Category Levels'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CategoryLevel', 'url'=>array('index')),
	array('label'=>'Create CategoryLevel', 'url'=>array('create')),
	array('label'=>'View CategoryLevel', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CategoryLevel', 'url'=>array('admin')),
);
?>

<h1>Update CategoryLevel <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>