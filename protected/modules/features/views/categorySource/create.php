<?php
/* @var $this CategorySourceController */
/* @var $model CategorySource */

$this->breadcrumbs=array(
	'Category Sources'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CategorySource', 'url'=>array('index')),
	array('label'=>'Manage CategorySource', 'url'=>array('admin')),
);
?>

<h1>Create CategorySource</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>