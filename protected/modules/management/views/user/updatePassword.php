<?php
/* @var $this ProfileFormController */
/* @var $model ProfileForm */

$this->pageTitle = "Users | Update";


?>


    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <?php
                        if (isset($_POST['updated'])) {
                            echo '<div class="alert alert-info">Updated</div>';
                        }
                        ?>
                        <div class="col-sm-9"><h2>
                                <?php $this->widget(
                                    'booster.widgets.TbButtonGroup',
                                    array(
                                        'size' => 'small',
                                        'context' => 'info',
                                        'buttons' => array(
                                            array(
                                                'label' => 'Update profile',
                                                'buttonType' => 'link',
                                                'url' => array('/management/user/update/', 'id' => $model_id->id)
                                            ),
                                        ),
                                    )
                                ); ?></h2></div>
                        <div class="col-sm-3" style="padding-top: 19px;text-align: left;">
                            <div class="col-sm-7"><?php echo Yii::app()->params['statement']['previousPage']; ?></div>
                            <div class="col-sm-5">
                                <?PHP
                                $this->widget(
                                    'booster.widgets.TbButtonGroup',
                                    array(
                                        'size' => 'small',
                                        'context' => 'info',
                                        'buttons' => array(
                                            array(
                                                'label' => 'Manage',
                                                'buttonType' => 'link',
                                                'url' => array('user/admin')
                                            ),
                                        ),
                                        /*'htmlOptions'=>array(
                                            'class'=>'pull-right	'
                                        )*/
                                    )
                                );

                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <?php $this->renderPartial('_passwordForm', array('model' => $model)); ?>
                    </div>
                </div>
            </div>
    </section>
