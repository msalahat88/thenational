<?php

class m161129_132347_news_id_parent extends CDbMigration
{
	public function up()
	{
	    $this->execute('ALTER TABLE `news` ADD `parent_news_id` INT(11) NULL DEFAULT NULL AFTER `sub_category_id`;
');
	}

	public function down()
	{
		echo "m161129_132347_news_id_parent does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}