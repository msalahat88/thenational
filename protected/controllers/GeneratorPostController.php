<?php

class GeneratorPostController extends BaseController
{

	private $details;
	private $category;
	private $model;
	private $trim_hash;
	private $hash_tag;
	private $platform;
	private $parent = null;
	private $creator;
	private $sub_category;
	private $source;
	private $news;
	private $PostQueue;

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index'),
				'users'=>array(Yii::app()->user->getState('type')),
                array('deny',  // deny all users
                    'users'=>array('*'),
                ),
			),

		);
	}
	public function actionIndex()
	{
		$this->model=new GeneratorPost();
		$this->performAjaxValidation();
		if(isset($_POST['GeneratorPost']))
		{

			if(isset($_POST['yt1']))
				$this->model->anyway = false;

			elseif(isset($_POST['yt2']))
				$this->model->anyway = true;

			$this->model->attributes=$_POST['GeneratorPost'];

			if(empty($this->model->getErrors())){
				$this->category = explode('www.emaratalyoum.com/',$this->model->link);
				if(is_array($this->category) && isset($this->category[1]) && !empty($this->category[1])){
					$this->category = explode('/',$this->category[1]);
					if(is_array($this->category) && isset($this->category[0]) && !empty($this->category[0])){
						$this->category = Category::model()->findByAttributes(array('url'=>trim(Yii::app()->params['feedUrl'].'/'.$this->category[0])));
						 if(!empty($this->category)){
							 $this->details = $this->get_details($this->model->link);
							 $this->AddNews();
						 }else{
							 $this->model->addError('link','Please check Url');
						 }
					}
				}else{
					$this->model->addError('link','Please check Url');
				}
			}

			if(empty($this->model->getErrors())){
				$this->redirect(array('postQueue/view', 'id' => $this->parent));
			}



		}

		$this->render('create',array(
			'model'=>$this->model,
		));
	}

	protected function performAjaxValidation()
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='generator-post-form')
		{
			echo CActiveForm::validate($this->model);
			Yii::app()->end();
		}
	}

	private function AddNews(){


		$this->news = News::model()->findByAttributes(array('link_md5' => $this->details['link_md5'], 'category_id' => $this->category->id));

		if($this->model->anyway)
			$this->news = array();

		if(empty($this->news)){
			$this->news = new News();

			$this->news->id = null;
			$this->news->link = $this->details['url'];
			$this->news->link_md5 = $this->details['link_md5'];
			$this->news->category_id = $this->category->id;
			$this->news->sub_category_id = null;
			$this->news->title = $this->details['title'];
			$this->news->column = $this->details['column'];
			$this->news->description = isset($this->details['description'])?$this->details['description']:null;
			$this->news->publishing_date = $this->details['date'];
			$this->news->created_at = date('Y-m-d H:i:s');
			$this->news->creator = isset($this->details['author'])?$this->details['author']:null;
			$this->news->shorten_url = $this->bitShort(urldecode($this->news->link));
			$this->news->schedule_date = $this->model->time;
			$this->news->setIsNewRecord(true);
			if($this->news->save(true))
			{
				$this->mediaNews($this->news->id,$this->details['image']);

				if(isset($this->details['gallary']))
					$this->mediaNews($this->news->id,$this->details['gallary']);
				$this->generator();
			}
		}else{

			$this->model->addError('link','Url exists .... ('.CHtml::link('Link',Yii::app()->baseUrl.'/postQueue/view/'.$this->news->id).')');
		}
	}

	private function mediaNews($id, $data)
	{

		$media = new MediaNews();
		if($data['type'] == 'image'){

			$media->command=false;
			$media->id=null;
			$media->news_id = $id;
			$media->media = $data['src'];
			$media->type=$data['type'];
			$media->setIsNewRecord(true);
			if(!$media->save()){

			}


		}elseif($data['type'] == 'gallery'){
			foreach ($data['src'] as $item) {
				$empty_media = MediaNews::model()->findByAttributes(array('news_id'=> $id,'media'=>$item));
				if(empty($empty_media)){
					$media->id=null;
					$media->news_id = $id;
					$media->media = $item;
					$media->type=$data['type'];
					$media->setIsNewRecord(true);
					if(!$media->save()){

					}
				}

			}
		}

	}

	private function get_details($url)
	{
		$html = new SimpleHTMLDOM();

		$html_data = $html->file_get_html($url);

		$data['url']= $url;

		$data['column']= null;

		$data['link_md5'] = md5($url);

		$data['image']['src'] =Yii::app()->params['domain'].'image/general.jpg';
		$data['image']['type']='image';

		if(isset($html_data->find('h1[class=articletitle]', 0)->innertext))
			$data['title']= $this->clear_tags($html_data->find('h1[class=articletitle]', 0)->innertext);
		else {
			if(isset($html_data->find('meta[property=og:title]',0)->content))
				$data['title'] =  $this->clear_tags($html_data->find('meta[property=og:title]',0)->content);
			else
				$data['title'] = 'blank title';
		}

		$data['date'] = $this->get_date($this->clear_tags($html_data->find('time[class=date]', 0)->datetime));
		if(isset($html_data->find('div[class=pull_right] ul li[itemprop=author]', 0)->innertext))
			$data['author'] = $this->clear_author($html_data->find('div[class=pull_right] ul li[itemprop=author]', 0)->innertext);
		else{
			if(isset($html_data->find('meta[name=author]',0)->content))
				$data['author'] = $this->clear_author($html_data->find('meta[name=author]',0)->content);
			elseif(isset($html_data->find('section[itemprop=author] meta[itemprop=name]',0)->content))
				$data['author'] = $html_data->find('section[itemprop=author] meta[itemprop=name]',0)->content;
			else
				$data['author'] = '';
		}

		$data['number_star']='';
		if(isset($html_data->find('span[id=star-raters-number]', 0)->innertext))
			$data['number_star'] = $this->clear_tags($html_data->find('span[id=star-raters-number]', 0)->innertext);

		$data['description']='';

		if(isset($html_data->find('div[id=articleContent]', 0)->innertext))
			$data['description']= $this->clear_tags($html_data->find('div[id=articleContent]', 0)->innertext);
		elseif(isset($html_data->find('div[id=article-body]', 0)->innertext))
			$data['description']= $this->clear_tags($html_data->find('div[id=article-body]', 0)->innertext);


		$data['description'] = str_replace('لقراءة مقالات سابقة للكاتب يرجى النقر على اسمه .', '',  $data['description']);
		$data['description'] = str_replace('يمكن إرسال سكيك على خدمة «خبرونا» ضمن تطبيقات «الإمارات اليوم» على الهواتف الذكية.', '',  $data['description']);
		$data['description'] = str_replace('نعتذر عن عدم نشر ملاحظات القراء غير المرفق بها اسم ورقم هاتف المرسل.', '',  $data['description']);
		$data['description'] = str_replace('sekeek@emaratalyoum.com', '',  $data['description']);

		foreach ($html_data->find('meta') as $item) {
			if (strpos($item->getAttribute('name'), 'twitter:image:src') !== false) {
				$data['image']['src'] = $item->getAttribute('content');
				$data['image']['type']='image';
			}
		}

		if($data['image']['src'] == 'http://cache.emaratalyoum.com/res/img/logo-1024x576.png'){
			$data['image']['src'] =Yii::app()->params['domain'].'image/general.jpg';
		}
		if(isset($html_data->find('article h3[itemprop=alternativeHeadline]',0)->innertext))
			$data['column']= $this->clear_tags($html_data->find('article h3[itemprop=alternativeHeadline]',0)->innertext);


		$gallary = $html_data->find('div[class=container_12] div[class=grid_8 pull_right content] article div[class=inlinegallery] div[class=articleinlinegallery] ul[class=slides] li img');
		$counter=0;
		foreach ($gallary as $item) {
			$data['gallary']['type']='gallery';
			$data['gallary']['src'][$counter] =  Yii::app()->params['feedUrl'].$item->src;
			$counter++;
		}

		$data['description'] = trim($data['description']);

		return $data;
	}

	private function clear_tags($string){

		$string = strip_tags($string);
		$string = str_replace('&nbsp;','',$string);
		$string = str_replace('&raquo;','',$string);
		$string = str_replace('&laquo;','',$string);
		$string = str_replace('&quot;','',$string);
		$string = str_replace('nbsp;','',$string);
		return $string;
	}

	private function get_date($date){

		return date('Y-m-d H:i:s',strtotime($date));

	}

	private function clear_author($string){

		$string = strip_tags($string);

		return $string;
	}

	private function generator(){

		$d = Hashtag::model()->findAll('deleted = 0');

		$this->trim_hash = array();

		$this->hash_tag = array();

		foreach ($d as $index=>$item) {
			$this->trim_hash[$index]=" #".str_replace(" ", "_",trim($item->title)).' ';
			$this->hash_tag[$index]=' '.trim($item->title).' ';
		}

		$this->parent = null;



		if(isset($this->category->title))
			$this->category = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_',$this->category->title));


		if(isset($this->sub_category->title))
			$this->sub_category = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_', isset($this->sub_category->title)));

		$this->source = Yii::app()->params['source'].' : ';

		if($this->category == '#'.'أعمدة')
			$this->source = Yii::app()->params['line'].' : ';


		$this->creator = false;

		if($this->news->creator == 'دبي - الإمارات اليوم')
			$this->creator =true;
		
		if($this->model->submit_to_all){
			foreach (Platform::model()->findAllByAttributes(array('deleted' => 0)) as $attribute) {
				$this->platform = $attribute;
				$this->post();
			}
		}else{

			foreach ($this->model->platforms as $item) {
				$this->platform = Platform::model()->findByPk($item);
				$this->post();
			}
		}

		$this->news->generated = 1;

		$this->news->save();
	}

	private function post(){

		$is_scheduled =1;

		$twitter_is_scheduled =false;

		$media= null;

		$temp = $this->get_template($this->platform,$this->news->category_id);

		$title = $this->news->title;

		$description = $this->shorten_point($this->news->description,'.');

		if($this->platform->title != 'Instagram'){
			$title = str_replace($this->hash_tag, $this->trim_hash, $this->news->title);
			$description = str_replace($this->hash_tag, $this->trim_hash, $this->shorten_point($this->news->description,'.'));
		}

		$title =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $title);
		$title = preg_replace('/\n(\s*\n){2,}/', "\n\n", $title);

		$description =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $description);
		$description = preg_replace('/\n(\s*\n){2,}/', "\n\n", $description);

		$shorten_url = $this->news->shorten_url;
		$full_creator = $this->source.$this->news->creator;
		if(!empty(strpos($this->news->creator,':')))
			$full_creator = $this->news->creator;

		$cre= $this->platform->title == 'Twitter'?($this->creator?$full_creator:null):$full_creator;


		$text = str_replace(array('[title]','[description]','[short_link]','[author]',),array($title,$description,$shorten_url,$cre),$temp['text']
		);

		if($this->platform->title == 'Facebook' or $this->platform->title == 'Twitter'){
			$text = str_replace('# ', '#', $text);
			$found = true;
			preg_match_all("/#([^\s]+)/", $text, $matches);
			if(isset($matches[0])){
				$matches[0] = array_reverse($matches[0]);
				$count = 0;
				foreach ($matches[0] as $hashtag){
					if($count>=2)
					{
						$found = false;
						$hashtag_without = str_replace('#','',$hashtag);
						$hashtag_without = str_replace('_',' ',$hashtag_without);
						$text = str_replace($hashtag,$hashtag_without,$text);

					}
					$count++;
				}

				if($count>=2) {
					$found = false;
				}
			}

			if($found){
				$text = str_replace(array('[section]', '[sub_section]',), array($this->category, isset($this->sub_category->title)?$this->sub_category->title:null,), $text);
			}else{
				$text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', '',), $text);
			}
		}elseif($this->platform->title=='Instagram'){
			$text = str_replace('# ', '#', $text);
			$text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', ''), $text);
		}


		$newsMedia = MediaNews::model()->findAll('(type = "image" or type = "gallery") and news_id = '.$this->news->id);
		if(!empty($newsMedia)){
			$index_media= rand(0,count($newsMedia)-1);
			if(isset($newsMedia[$index_media])){
				$media = $newsMedia[$index_media]->media;
			}
		}
		if($this->platform->title == 'Twitter'){

			$text_twitter =$text;

			if($temp['type'] == 'Preview')
				if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
					$text =$text.PHP_EOL.$this->news->shorten_url;

			if($this->getTweetLength($text_twitter,$temp['type'] == 'Image'?true:false,$temp['type'] == 'Video'?true:false) > 141) {
				$is_scheduled = 0;
				$twitter_is_scheduled = true;
			}else{
				$twitter_is_scheduled =false;
				$text = $text_twitter.PHP_EOL;
				if($temp['type'] == 'Preview')
					if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
						$text .=PHP_EOL.$this->news->shorten_url;
			}

		}else{
			if($twitter_is_scheduled){
				if(!$is_scheduled){
					$is_scheduled = 1;
				}
			}
		}
		$text = str_replace('# #', '#', $text);
		$text = str_replace('##', '#', $text);
		$text = str_replace('&#8220;', '', $text);
		$text = str_replace('&#8221;', '', $text);
		$text = str_replace('&#8230;', '', $text);
		$text = str_replace('#8211;', '', $text);
		$text = str_replace('&#8211;', '', $text);
		$text = str_replace('&nbsp;', '', $text);
		$text = str_replace('&#160;', '', $text);
		$text = str_replace('&#8211;', '', $text);
		$text = str_replace('&ndash;', '', $text);
		if($this->platform->title != 'Instagram'){
			$text =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $text);
			$text = preg_replace('/\n(\s*\n){2,}/', "\n\n", $text);
		}

		$this->PostQueue = new PostQueue();
		$this->PostQueue->setIsNewRecord(true);
		$this->PostQueue->id= null;
		$this->PostQueue->type = $temp['type'];
		$this->PostQueue->post = $text;
		$this->PostQueue->schedule_date = $this->news->schedule_date;
		$this->PostQueue->catgory_id =  $this->news->category_id;
		$this->PostQueue->main_category_id =  $this->news->category_id;
		$this->PostQueue->link = $this->news->shorten_url;
		$this->PostQueue->is_posted = 0;
		$this->PostQueue->news_id = $this->news->id;
		$this->PostQueue->post_id =null;
		$this->PostQueue->media_url =$media;
		$this->PostQueue->settings ='general';
		$this->PostQueue->is_scheduled =$is_scheduled;
		$this->PostQueue->platform_id =$this->platform->id;
		$this->PostQueue->generated ='auto';
		$this->PostQueue->created_at =date('Y-m-d H:i:s');

		if($this->parent == null){
			$this->PostQueue->parent_id =null;
			if($this->PostQueue->save())
				$this->parent = $this->PostQueue->id;

		}else{
			$this->PostQueue->parent_id =$this->parent;
			$this->PostQueue->save();
		}


	}

	private function get_template($platform,$category){

		$temp  = PostTemplate::model()->findByAttributes(array(
			'platform_id'=>$platform->id,
			'catgory_id'=>$category,
		));
		if(!empty($temp))
			return $temp;
		$cond = new CDbCriteria();
		$cond->order = 'RAND()';
		$cond->condition='( ( platform_id = '.$platform->id.' or platform_id is NUll ) and  catgory_id is NULL )';
		$temp  = PostTemplate::model()->find($cond);

		if(!empty($temp))
			return $temp;

		return Yii::app()->params['templates'];
	}

	public function shorten_point($input, $point = '.') {
		$input_ex = explode($point,$input);
		$data = array_chunk($input_ex,2);
		if(is_array($data)){

			return (implode('. '.PHP_EOL,$data[0])).'. ';
		}
		return $input;
	}


}