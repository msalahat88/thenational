<?php
/* @var $this InstagramAccountController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Instagram Accounts',
);

$this->menu=array(
	array('label'=>'Create InstagramAccount', 'url'=>array('create')),
	array('label'=>'Manage InstagramAccount', 'url'=>array('admin')),
);
?>

<h1>Instagram Accounts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
