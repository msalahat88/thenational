<?php
/* @var $this PostTemplateController */
/* @var $model PostTemplate */

$this->pageTitle = "Post template | Admin";

$this->breadcrumbs = array(
    'Post Templates' => array('admin'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List PostTemplate', 'url' => array('index')),
    array('label' => 'Create PostTemplate', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('#form-visible').change(function(){
	$(this).submit();
	return true;
});
$('.search-form form').submit(function(){
	$('#post-template-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<style>
    .showhideitem {
        display: block;
    }
</style>
<style>
    .checkbox {
        display: block;
        min-height: 0px;
        margin-top: 4px;
        margin-bottom: 0px;
        padding-left: 17px;
    }
    .input-group-addon input[type=radio], .input-group-addon input[type=checkbox] {
        margin-top: 0;
    }
    .radio input[type=radio], .radio-inline input[type=radio], .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {
        float: left;
        margin-left: -16px;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="col-md-9"> <?PHP
                        $this->widget(
                            'booster.widgets.TbButtonGroup',
                            array(
                                'size' => 'small',
                                'context' => 'info',
                                'buttons' => array(
                                    array(
                                        'label' => 'Create',
                                        'buttonType' =>'link',
                                        'url' => array('/PostTemplate/create')
                                    ),
                                ),
                            )
                        );
                        ?></div>
                    <div class="col-md-3" style=" text-align: left;">
                        <?php echo Yii::app()->params['statement']['previousPage']; ?>



                    </div>
                </div>
                <div class="box-body">
                    <?php
                    //echo $this->renderPartial('_search',array('model'=>$model),true)
                    ?>
                    <?PHP
                    $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
                        'action'=>Yii::app()->createUrl($this->route),
                        'method'=>'get',
                        'id'=>'form-visible',
                    ));
                    ?>
                    <div class="col-sm-2 pull-left page-sizes"  >

                        <?php echo $form->dropDownListGroup(
                            $model,
                            'pagination_size',
                            array(

                                'widgetOptions'=>array(
                                    'data'=>$model->pages_size(),
                                    'htmlOptions'=>array(

                                    ),
                                ),
                                'hint'=>''
                            )
                        ); ?>
                    </div>
                    <?php
                    $this->endWidget();
                    $this->widget('booster.widgets.TbGridView', array(
                        'id' => 'post-template-grid',
                        'type' => 'striped bordered condensed',
                        'template' => '{pager}{items}{summary}{pager}',
                        'enablePagination' => true,
                        'pager' => array(
                            'class' => 'booster.widgets.TbPager',
                            'nextPageLabel' => 'Next',
                            'prevPageLabel' => 'Previous',
                            'htmlOptions' => array(
                                'class' => 'pull-right'
                            )
                        ),
                        'htmlOptions' => array(
                            'class' => 'table-responsive'
                        ),
                        'dataProvider' => $model->search(),
                        'filter'=>$model,
                         'columns' => array(
                       /*     array(
                                'name'=>'id',
                                'visible'=>$model->visible_id?true:false,
                            ),*/
                            array(
                                'name' => 'type',
                                'filter' =>Yii::app()->params['rule_template']['facebook'],

                                'type' => 'raw',
                                'value' => '$data->type',
                                'visible'=>$model->visible_type?true:false,

                            ),
                            array(
                                'name' => 'platform_id',
                                'filter' => CHtml::listData(Platform::model()->findAll(), 'id', 'title'),
                                'htmlOptions' => array('width' => '100px'),
                                'visible'=>$model->visible_platform_id?true:false,
                                'value' => function ($data) {
                                    if($data->platform_id == 0){
                                        echo 'All platforms';
                                    }else
                                    echo CHtml::tag("i", array("class" => "fa fa-" . strtolower($data->platform->title), "aria-hidden" => "true", "style" => "font-size:20px;")) . " ";
                                }

                            ),
                            array(
                                'name' => 'catgory_id',
                                'filter' => CHtml::listData(Category::model()->findAll('deleted=0'), 'id', 'title'),
                                'value' => '$data->catgory_id == 0? "All sections" :$data->category->title',
                                'visible'=>$model->visible_catgory_id?true:false,

                            ),
                            array(
                                'name' => 'text',
                                'type' => 'raw',
                                'htmlOptions' => array('width' => '500px'),
                                'visible'=>$model->visible_text?true:false,
                                'value'=>'str_replace(PHP_EOL,"<br>","$data->text")',

                            ),/*array(
                                'name' => 'created_at',
                                 'visible'=>$model->visible_created_at?true:false,

                            ),*/
                            array(

                                'class' => 'booster.widgets.TbButtonColumn',
                                'header' => 'Options',
                                'template' => '{view}{update}{delete}',
                                'buttons' => array(
                                    'view' => array(
                                        'label' => 'View',
                                        'icon' => 'fa fa-eye',
                                    ),
                                    'update' => array(
                                        'label' => 'Update',
                                        'icon' => 'fa fa-pencil-square-o',
                                    ),
                                    'delete' => array(
                                        'label' => 'Delete',
                                        'icon' => 'fa fa-times',
                                    ),
                                )
                            ),
                        ))); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
$createLink = Yii::app()->createUrl('postQueue/main', array('#' => 'guider=ten'));

$this->widget('ext.eguiders.EGuider', array(
        'id' => 'managefirst',
        'title' => 'Type',
        'next' => 'second',
        'buttons' => array(
            array('name' => 'Previous', 'classString' => 'tourcolor', 'onclick' => "js:function(){  document.location = '$createLink';}"),

            array(
                'name' => 'Next',

            ),

            array(
                'name' => 'Exit',
                'onclick' => "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description' => 'displays the assigned type',
        'overlay' => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo' => '#post-template-grid_c0',
        'position' => 1,
        'xButton' => true,
        'onShow' => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
        'id' => 'second',
        'title' => 'Platform',
        'next' => 'third',
        'buttons' => array(
            array(
                'name' => 'Previous',
                'onclick' => "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('managefirst');}"
            ),
            array(
                'name' => 'Next',

            ),

            array(
                'name' => 'Exit',
                'onclick' => "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description' => 'Displays the assigned platform',
        'overlay' => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo' => '#post-template-grid_c1',
        'position' => 1,
        'xButton' => true,
        'onShow' => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
        'id' => 'third',
        'title' => 'Category',
        'next' => 'fourth',
        'buttons' => array(
            array(
                'name' => 'Previous',
                'onclick' => "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}"
            ),
            array(
                'name' => 'Next',

            ),

            array(
                'name' => 'Exit',
                'onclick' => "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description' => 'Displays the assigned category',
        'overlay' => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo' => '#post-template-grid_c2',
        'position' => 1,
        'xButton' => true,
        'onShow' => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
        'id' => 'fourth',
        'title' => 'Text',
        'next' => 'fifth',
        'buttons' => array(
            array(
                'name' => 'Previous',
                'onclick' => "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('third');}"
            ),
            array(
                'name' => 'Next',

            ),

            array(
                'name' => 'Exit',
                'onclick' => "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description' => 'Displays the template properties',
        'overlay' => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo' => '#post-template-grid_c3',
        'position' => 1,
        'xButton' => true,
        'onShow' => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>

<?php

$this->widget('ext.eguiders.EGuider', array(
        'id' => 'fifth',
        'title' => 'Options',
        'next' => 'sex',
        'buttons' => array(
            array(
                'name' => 'Previous',
                'onclick' => "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fourth');}"
            ),
            array(
                'name' => 'Next',
                'onclick' => "js:function(){ $('#yw1').addClass('showhideitem'); guiders.hideAll(); $('.highlight pre').hide(); guiders.show('sex'); }"
            ),

            array(
                'name' => 'Exit',
                'onclick' => "js:function(){guiders.hideAll(); }"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description' => 'The options you can make for this item(View,Update and Delete)',
        'overlay' => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo' => '#post-template-grid_c4',
        'position' => 1,
        'xButton' => true,
        'onShow' => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$createTemplate = Yii::app()->createUrl('managePostTemplate/index', array('#' => 'guider=first'));

$this->widget('ext.eguiders.EGuider', array(
        'id' => 'sex',
        'title' => 'Options',
        'next' => 'seven',
        'buttons' => array(
            array(
                'name' => 'Previous',
                'onclick' => "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fifth');$('#yw1').removeClass('showhideitem') }"
            ),
            array(
                'name' => 'Next',
                'onclick' => "js:function(){ $('#yw1').removeClass('showhideitem'); guiders.hideAll(); $('.highlight pre').hide(); guiders.show('seven'); }"
            ),
            array('name' => 'Create Tour', 'classString' => 'tourcolor', 'onclick' => "js:function(){   document.location = '$createTemplate'; }"),


            array(
                'name' => 'Exit',
                'onclick' => "js:function(){guiders.hideAll(); $('#yw1').removeClass('showhideitem') }"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description' => 'Create new template',
        'overlay' => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo' => '#yw1',
        'position' => 9,
        'xButton' => false,
        'onShow' => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
        'id' => 'seven',
        'title' => 'Option view',
        'next' => 'eight',
        'buttons' => array(
            array(
                'name' => 'Previous',
                'onclick' => "js:function(){ $('#yw1').addClass('showhideitem'); guiders.hideAll(); $('.highlight pre').hide(); guiders.show('sex'); }"
            ),
            array(
                'name' => 'Next',
            ),

            array(
                'name' => 'Exit',
                'onclick' => "js:function(){guiders.hideAll(); }"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description' => 'This option to view item alone',
        'overlay' => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo' => '.fa-eye',
        'position' => 9,
        'xButton' => true,
        'onShow' => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$continueTour = Yii::app()->createUrl('postQueue/main', array('#' => 'guider=ten'));

$this->widget('ext.eguiders.EGuider', array(
        'id' => 'nine',
        'title' => 'Option Delete',
        'next' => 'end',
        'buttons' => array(
            array(
                'name' => 'Previous',
                'onclick' => "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('eight');}"
            ),
            array(
                'name' => 'Next',
            ),

            array('name' => 'Continue ', 'classString' => 'tourcolor', 'onclick' => "js:function(){  document.location = '$continueTour';}"),


            array(
                'name' => 'Exit',
                'onclick' => "js:function(){guiders.hideAll(); }"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description' => 'This option to delete this item',
        'overlay' => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo' => '.fa-times',
        'position' => 9,
        'xButton' => true,
        'onShow' => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
        'id' => 'eight',
        'title' => 'Option edit',
        'next' => 'nine',
        'buttons' => array(
            array(
                'name' => 'Previous',
                'onclick' => "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('seven');}"
            ),
            array(
                'name' => 'Next',
            ),


            array(
                'name' => 'Exit',
                'onclick' => "js:function(){guiders.hideAll(); }"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description' => 'This option to edit this item',
        'overlay' => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo' => '.fa-pencil-square-o',
        'position' => 9,
        'xButton' => true,
        'onShow' => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<!--EndTour-->