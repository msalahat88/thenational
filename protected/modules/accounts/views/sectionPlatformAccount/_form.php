<?php
/* @var $this InstagramAccountController*/
/* @var $model InstagramAccount */
/* @var $form TbActiveForm */
?>
<style type="text/css">
	.form-horizontal .control-label {
		text-align: left;
	}
</style>

<div class="form" style="padding-top: 30px">

	<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
		'id'=>'section-platform-account-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>true,
		'type' => 'horizontal',
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
	));
	$field = 'col-sm-10';
	$label = 'col-sm-2';

	?>
	<div class="col-sm-12" >

		<?php echo $form->dropDownListGroup($model,'section_id',array(
			'labelOptions' => array(
				'class' => 'col-sm-2',
			),
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-10',
			),
			'widgetOptions'=>array(
				'data'=>CHtml::listData(Category::model()->findAll(),'id','title')
			)
		)); ?>
		<?php echo $form->dropDownListGroup($model,'platform_id',array(
			'labelOptions' => array(
				'class' => 'col-sm-2',
			),
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-10',
			),
			'widgetOptions'=>array(
				'data'=>CHtml::listData(Platform::model()->findAll(),'id','title')
			)
		)); ?>
		<?php echo $form->dropDownListGroup($model,'account_id',array(
			'labelOptions' => array(
				'class' => 'col-sm-2',
			),
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-10',
			),
			'widgetOptions'=>array(
				'data'=>CHtml::listData($model_platforms,'id','link_page'),
			)
		)); ?>

	</div>


	<div class="form-actions  pull-right" style="margin-bottom: 20px;margin-right:20px;">
		<?php $this->widget(
			'booster.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'context' => 'primary',
				'label' => $model->isNewRecord ? 'Create' : 'Save',
			)
		); ?>

	</div>
	<?php $this->endWidget(); ?>

</div><!-- form -->