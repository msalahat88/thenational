<?php

class m161208_112611_new_filler extends CDbMigration
{
	public function up()
	{
	    $this->execute('ALTER TABLE `post_queue` CHANGE `generated` `generated` ENUM(\'auto\',\'manual\',\'clone\',\'thematic\',\'pinned\',\'filler\') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT \'auto\';
');
	}

	public function down()
	{
		echo "m161208_112611_new_filler does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}