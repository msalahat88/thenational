<?php
class TestRssController extends FeaturesBaseController{
    public function actionIndex(){
        $categories = CategorySource::model()->findAll('type="rss"');
        $counter=0;
        echo "<pre>";
        $data=array();
        foreach($categories as $category){
            $rss = file_get_contents($category->url);
            if(empty(trim($rss))){
                continue;
            }
            $xml = @simplexml_load_string($rss);
            if($xml){
                if(isset($xml->channel->item)){
                    foreach($xml->channel->item as $item){
                        $namespaces = $item->getNameSpaces(true);
                        $dc = $item->children($namespaces['dc']);
                        $dc_date = $item->children($namespaces['dc']);

                        $e_content = $item->children("content", true);
                        if (isset($e_content->encoded))
                            $e_encoded = (string)$e_content->encoded;


                        if(isset($dc))
                        $data['creator'][$counter] = (string)$dc->creator;

                        if (isset($item->title))
                            $data['title'][$counter] = strip_tags((string)$item->title);

                        if(isset($item->link)){
                                $data['link'][$counter] = (string)$item->link;
                                $data['link_md5'][$counter] = md5((string)$item->link);
                        }
                        if (isset($item->description))
                            $data['description'][$counter] = strip_tags((string)$item->description);

                        if (isset($e_encoded))
                            $data['body_description'][$counter] = strip_tags($e_encoded);

                        if (isset($item->pubDate))
                            $pub_date = (string)$item->pubDate;
                        elseif(isset($dc_date))
                            $pub_date = $dc_date->date;

                        if (isset($pub_date))
                            $data['publishing_date'][$counter] = date('Y-m-d H:i:s', strtotime(trim($pub_date)));

                        list($title,$description,$body_description,$video,$image_og,$gallery,$publishing_date,$author) = $this->get_details($data['link'][$counter]);
                        $data['title2'][$counter] = $title;
                        $data['description2'][$counter] = $description;
                        $data['body_description2'][$counter] = $body_description;
                        $data['image_og2'][$counter] = $image_og;
                        $data['video2'][$counter] = $video;
                        $data['gallery2'][$counter] = $gallery;
                        $data['publishing_date2'][$counter] = $publishing_date;
                        $data['author2'][$counter] = $author;
                        $counter++;
                    }
                }
            }

        }
        print_r($data);
    }
    private function get_details($link)
    {
        $html = new SimpleHTMLDOM();
        $html_url = $html->file_get_html($link);
        $page_details = PageDetails::model()->findAll();
        $author=null;
        $gallery = null;
        $publishing_date = null;
        $body_description = null;
        $description = null;
        $image = Yii::app()->params['domain']. 'image/general.jpg';;
        $video =null;
        $title=null;
        foreach($page_details as $detail) {

            //Author
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'creator') {
                $author = $html_url->find($detail->predication, 0)->content;

            }
            //End author

            //title
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'title') {
                $title = $html_url->find($detail->predication, 0)->plaintext;
            }
            //End title


            //Body desc
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'body_description') {
                $body_desc = $html_url->find($detail->predication);
                if (isset($body_desc)) {
                    foreach ($body_desc as $bodies) {
                        $body_description .= $bodies->plaintext;
                    }
                }
            }

            //End body desc

            //Description
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'description') {

                $description = $html_url->find($detail->predication, 0)->content;
            }elseif (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'description') {
                $description = $html_url->find($detail->predication, 0)->plaintext;

            }
                //End Description

            //Publishing date
            if (isset($html_url->find($detail->predication, 0)->datetime) and !empty($html_url->find($detail->predication, 0)->datetime) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $html_url->find($detail->predication, 0)->datetime;
            }
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $html_url->find($detail->predication, 0)->plaintext;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $html_url->find($detail->predication, 0)->content;
            }
            //End publishing date

            //All media
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'all_media') {
                $script = $html_url->find($detail->predication);
                if (!empty($script)) {
                    foreach ($script as $sc) {
                        $media = (array)json_decode($sc->innertext);
                    }
                    if (!empty($media)) {
                        if (isset($media['video'])) {
                            $video = $media['videos'];
                        }
                        if (isset($media['images'])) {
                            foreach ($media['images'] as $gall) {
                                $image = 'http://www.wam.ae' . $media['images'][0];
                                if ($gall != $media['images'][0])
                                    $gallery[] = 'http://www.wam.ae' . $gall;
                            }
                        }
                    }
                }
            }
            //End all media

            //image
            if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                    $image = $html_url->find($detail->predication, 0)->src;

                }else
                    $image = 'http://www.emaratalyoum.com'.$html_url->find($detail->predication, 0)->src;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->content, 'http') === 0)
                    $image = $html_url->find($detail->predication, 0)->content;
                else
                    $image = 'http://www.emaratalyoum.com'.$html_url->find($detail->predication, 0)->content;

            }
            //end image

            //video
            if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'video') {
                $video = $html_url->find($detail->predication, 0)->src;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'video') {
                $video = $html_url->find($detail->predication, 0)->content;

            }
            //end video

            //gallery
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'gallery') {
                $gall = $html_url->find($detail->predication);
                foreach ($gall as $item) {
                    if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                        if($image != $item->src)
                            $gallery[] =$item->src;
                    }else {
                        if ($image != 'http://www.emaratalyoum.com' . $item->src)
                            $gallery[] = 'http://www.emaratalyoum.com' .$item->src;
                    }
                }

            }
            //End gallery
        }
        return array($title,$description,$body_description,$video,$image,$gallery,$publishing_date,$author);
    }

}