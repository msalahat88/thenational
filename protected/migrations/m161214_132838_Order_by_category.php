<?php

class m161214_132838_Order_by_category extends CDbMigration
{
	public function up()
	{
	    $this->execute('ALTER TABLE `category` ADD `order_by` INT(11) NULL DEFAULT NULL AFTER `url_rss`;
');
	}

	public function down()
	{
		echo "m161214_132838_Order_by_category does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}