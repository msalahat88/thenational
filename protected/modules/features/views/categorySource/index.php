<?php
/* @var $this CategorySourceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Category Sources',
);

$this->menu=array(
	array('label'=>'Create CategorySource', 'url'=>array('create')),
	array('label'=>'Manage CategorySource', 'url'=>array('admin')),
);
?>

<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9"></div>
					<div class="col-sm-3" style=" text-align: left;">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>





					</div>
				</div>
				<div class="box-body">
					<?php
					$this->widget(
						'booster.widgets.TbTabs',
						array(
							'type' => 'tabs', // 'tabs' or 'pills'
							'tabs' => array(
								array(
									'label' => 'Category_source',
									'buttonType'=>'url',
									'active' => true,

									'url'=>Yii::app()->baseUrl.'/features/categorySource/index'
								),
								array('label' => 'Category Level',
									'buttonType'=>'url',
									'url'=>Yii::app()->baseUrl.'/features/categoryLevel/index'
								),
								array('label' => 'Page details',
									'buttonType'=>'url',
									'url'=>Yii::app()->baseUrl.'/features/pageDetails/index'
								),

							),
						)
					);
					?>
					<h2>Quick create</h2>
					<?php $this->renderPartial('_form', array('model'=>$model)); ?>

					<?php
					$this->widget('booster.widgets.TbGridView', array(
						'id'=>'hashtag-grid',
						'type' => 'striped bordered condensed',
						'template' => '{pager}{items}{summary}{pager}',
						'enablePagination' => true,
						'pager' => array(
							'class' => 'booster.widgets.TbPager',
							'nextPageLabel' => 'Next',
							'prevPageLabel' => 'Previous',
							'htmlOptions' => array(
								'class' => 'pull-right'
							)
						),
						'htmlOptions' => array(
							'class' => 'table-responsive'
						),
						'dataProvider' => $models->search(),
						'filter' => $models,
						'columns' => array(
							/*	array(
                                    'name'=>'id',
                                    'visible'=>$model->visible_id?true:false,
                                ),*/
							array(
								'name' => 'category',
							),


							array(
								'name'=>'url',

							),
							array(
								'name'=>'type',

							),
							array(
								'name'=>'lang',

							),
							array(
								'name'=>'deleted',

							),
							array(
								'name'=>'active',

							),

							/*	array(
                                    'name'=>'created_at',
                                    'visible'=>$model->visible_created_at?true:false,

                                ),*/
							array(
								'class' => 'booster.widgets.TbButtonColumn',
								'header' => 'Options',
								'template' => '{view}{update}{delete}',
								'buttons' => array(
									'view' => array(
										'label' => 'View',
										'icon' => 'fa fa-eye',
									),
									'update' => array(
										'label' => 'Update',
										'icon' => 'fa fa-pencil-square-o',
									),
									'delete' => array(
										'label' => 'Delete',
										'icon' => 'fa fa-times',
									),
								)
							),
						)));?>

				</div>
			</div>
		</div>
</section>