<?php
/* @var $this HashtagController */
/* @var $model Hashtag */

$this->pageTitle = "Hashtag | Admin";

$this->breadcrumbs=array(
	'Hashtags'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Hashtag', 'url'=>array('index')),
	array('label'=>'Create Hashtag', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('#form-visible').change(function(){
	$(this).submit();
	return true;
});
$('.search-form form').submit(function(){
	$('#hashtag-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<style>
    .showhideitem{
        display: block;
    }
</style>
<style>
	.checkbox {
		display: block;
		min-height: 0px;
		margin-top: 4px;
		margin-bottom: 0px;
		padding-left: 17px;
	}
	.input-group-addon input[type=radio], .input-group-addon input[type=checkbox] {
		margin-top: 0;
	}
	.radio input[type=radio], .radio-inline input[type=radio], .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {
		float: left;
		margin-left: -16px;
	}
</style>
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9"></div>
					<div class="col-sm-3" style=" text-align: left;">
					 <?php echo Yii::app()->params['statement']['previousPage']; ?>





					</div>
				</div>
				<div class="box-body">
					<h2>Quick create</h2>
					<?php $this->renderPartial('_form', array('model'=>$models)); ?>

					<?PHP //echo $this->renderPartial('_search',array('model'=>$model),true) ?>

					<?PHP
					$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
						'action'=>Yii::app()->createUrl($this->route),
						'method'=>'get',
						'id'=>'form-visible',
					));
					?>
					<div class="col-sm-2 pull-left page-sizes"  >

						<?php echo $form->dropDownListGroup(
							$model,
							'pagination_size',
							array(

								'widgetOptions'=>array(
									'data'=>$model->pages_size(),
									'htmlOptions'=>array(

									),
								),
								'hint'=>''
							)
						); ?>
					</div>
					<?php
					$this->endWidget();
					$this->widget('booster.widgets.TbGridView', array(
						'id'=>'hashtag-grid',
						'type' => 'striped bordered condensed',
						'template' => '{pager}{items}{summary}{pager}',
						'enablePagination' => true,
						'pager' => array(
							'class' => 'booster.widgets.TbPager',
							'nextPageLabel' => 'Next',
							'prevPageLabel' => 'Previous',
							'htmlOptions' => array(
								'class' => 'pull-right'
							)
						),
						'htmlOptions' => array(
							'class' => 'table-responsive'
						),
						'dataProvider' => $model->search(),
						'filter' => $model,
 						'columns' => array(
						/*	array(
								'name'=>'id',
								'visible'=>$model->visible_id?true:false,
							),*/
							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name' => 'title',
								'sortable' => true,
								'editable' => array(
									'url' => $this->createUrl('hashtag/edit'),
									'placement' => 'right',
									'inputclass' => 'span1'
								),
								'visible'=>$model->visible_title?true:false,
							),


							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name'=>'category_id',
								'type'=>'raw',
								'filter'=>CHtml::listData(Category::model()->findAll('deleted=0'),'id','title'),
								'value'=>'isset($data->category->title)?$data->category->title:"No category"',
								'visible'=>$model->visible_category_id?true:false,
								'editable' => array(
									'type' => 'select2',
									'url' => $this->createUrl('hashtag/edit'),
									'source' =>CHtml::listData(Category::model()->findAll(),'id','title'),
									'placement' => 'right',
									'inputclass' => 'span1'
								),
							),
						/*	array(
								'name'=>'created_at',
								'visible'=>$model->visible_created_at?true:false,

							),*/
 							array(
								'class' => 'booster.widgets.TbButtonColumn',
								'header' => 'Options',
								'template' => '{view}{update}{delete}',
								'buttons' => array(
									'view' => array(
										'label' => 'View',
										'icon' => 'fa fa-eye',
									),
									'update' => array(
										'label' => 'Update',
										'icon' => 'fa fa-pencil-square-o',
									),
									'delete' => array(
										'label' => 'Delete',
										'icon' => 'fa fa-times',
									),
								)
							),
						)));?>

				</div>
			</div>
		</div>
</section>

<!-- Tour -->
<?php
$createLink = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=fourteen'));

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'firstHashtag',
        'title'        => 'Title',
        'next'         => 'second',
        'buttons'      => array(
            array('name'=>'Previous','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$createLink';}"),

            array(
                'name'   => 'Next',

            ),

            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Title of the Hashtag',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#hashtag-grid_c0',
        'position'      => 1,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>


<?php

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'second',
        'title'        => 'Category',
        'next'         => 'third',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('firstHashtag');}"
            ),
            array(
                'name'   => 'Next',
                'onclick'=> "js:function(){ $('#yw1').addClass('showhideitem'); guiders.hideAll(); $('.highlight pre').hide(); guiders.show('third'); }"
            ),

            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Category that this hashtag belongs to',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#hashtag-grid_c1',
        'position'      => 1,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$createTemplate = Yii::app()->createUrl('hashtag/create',array('#' => 'guider=first'));

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'third',
        'title'        => 'Action create',
        'next'         => 'fourth',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');$('#yw1').removeClass('showhideitem') }"
            ),
            array(
                'name'   => 'Next',
                'onclick'=> "js:function(){ $('#yw1').removeClass('showhideitem'); guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fourth'); }"
            ),
            array('name'=>'Create Tour','classString' => 'tourcolor','onclick'=> "js:function(){   document.location = '$createTemplate'; }"),


            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll(); $('#yw1').removeClass('showhideitem') }"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Create new template',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '#yw1',
        'position'      => 9,
        'xButton'       => false,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>

<?php

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'fourth',
        'title'        => 'Option view',
        'next'         => 'fifth',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){ $('#yw1').addClass('showhideitem'); guiders.hideAll(); $('.highlight pre').hide(); guiders.show('third'); }"
            ),
            array(
                'name'   => 'Next',

            ),

            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'View this item alone',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '.fa-eye',
        'position'      => 9,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'fifth',
        'title'        => 'Option edit',
        'next'         => 'six',
        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fourth');}"
            ),
            array(
                'name'   => 'Next',

            ),

            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'update this item',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '.fa-pencil-square-o',
        'position'      => 9,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php
$continueTour = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=fifteen'));

$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'six',
        'title'        => 'Option remove',

        'buttons'      => array(
            array(
                'name'   => 'Previous',
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fifth');}"
            ),

            array('name'=>'Continue ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$continueTour';}"),

            array(
                'name'   => 'Exit',
                'onclick'=> "js:function(){guiders.hideAll();}"
            )
        ),
        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Delete this item',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        'attachTo'      => '.fa-times',
        'position'      => 9,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>

<!--endTour-->
