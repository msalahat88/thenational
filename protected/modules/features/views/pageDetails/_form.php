<?php
/* @var $this PageDetailsController */
/* @var $model PageDetails */
/* @var $form TbActiveForm */
?>

<div class="form">

	<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
		'id'=>'page-details-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>true,
		'type' => 'horizontal',
	)); ?>
	<div class="col-sm-3">

		<?php
		if($model->IsNewRecord){
			?>
			<?php echo $form->select2Group($model,'source_id',array(
				'widgetOptions'=>array(
					'htmlOptions' => array(
						'multiple' => 'multiple',
					),
					'data'=>CHtml::listData(Category::model()->getrelat(),'id','title','type'),
				)
			)); } else{
			echo $form->dropDownListGroup($model,'source_id',array(
				'widgetOptions'=>array(
					'data'=>CHtml::listData(Category::model()->getrelat(),'id','title','type'),
				)
			));

		}?>
	</div>
	<div class="col-sm-3">

		<?php echo $form->dropDownListGroup($model,'page_type_id',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(PageTypes::model()->findAll(),'id','title'),
			)
		)); ?>
	</div>
	<div class="col-sm-3">

		<?php echo $form->textFieldGroup($model,'predication'); ?>

	</div>

	<div class="col-sm-3">
		<?php $this->widget(
			'booster.widgets.TbButton',
			array(
				'htmlOptions'=>array(
					'id'=>'test'
				),
				'context' => 'danger',
				'label' => 'Test'
			)
		); ?>
		<div class="form-actions  pull-right" >
			<?php $this->widget(
				'booster.widgets.TbButton',
				array(
					'htmlOptions'=>array(
						'disabled'=>true,
						'id'=>'disabled_button'
					),
					'buttonType' => 'submit',
					'context' => 'primary',
					'label' => $model->isNewRecord ? 'Create' : 'Save'
				)
			); ?>	<?php $this->widget(
				'booster.widgets.TbButton',
				array(
					'htmlOptions'=>array(
						'name'=>'submit_to_all'
					),
					'buttonType' => 'submit',
					'context' => 'warning',
					'label' => $model->isNewRecord ? 'submit to all without test' : 'Save'
				)
			); ?>


		</div>
	</div>
	<?php $this->endWidget(); ?>

</div><!-- form -->
