<?PHP /* @var $data PostQueue */ ?>
<?PHP



if($data->type == 'Image'){


    $this->renderPartial('image',array('data'=>$data));

}elseif($data->type=='Text'){
    $this->renderPartial('text',array('data'=>$data));
}elseif($data->type=='Video'){
    $this->renderPartial('video',array('data'=>$data));

}elseif($data->type == 'Preview'){
    if($data->generated == 'filler'){
        $data->get_data_from_url($data);
    }
    $this->renderPartial('preview',array('data'=>$data));
}elseif($data->type == 'Multiple_Image') {
    $this->renderPartial('Multiple_Image', array('data' => $data));
}elseif($data->type == 'Albums') {
    $this->renderPartial('Multiple_Image', array('data' => $data));
}
?>