<?php

class m161129_180405_make_index_date_post_queue extends CDbMigration
{
	public function up(){

        $this->execute("ALTER TABLE `post_queue` ADD INDEX(`schedule_date`);");
	}

	public function down()
	{
		echo "m161129_180405_make_index_date_post_queue does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}