<?php
/* @var $this CategoryLevelController */
/* @var $model CategoryLevel */

$this->breadcrumbs=array(
	'Category Levels'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CategoryLevel', 'url'=>array('index')),
	array('label'=>'Manage CategoryLevel', 'url'=>array('admin')),
);
?>

<h1>Create CategoryLevel</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>