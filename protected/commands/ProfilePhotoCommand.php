<?php
class ProfilePhotoCommand extends BaseCommand{


    public function run($args){
        $this->TimeZone();
        $profile = $this->GetPhotoFacebook();
        $this->Facebook($profile);
        $profile = $this->GetPhotoTwitter();
        $this->Twitter($profile);
    }

    private function Facebook($profile){
        if(!empty($profile)){
            list($facebook,$PAGE_TOKEN)=$this->Load();

            if(!empty($profile->media_url)){
                $content = file_get_contents($profile->media_url);
                file_put_contents(Yii::app()->params['webroot'].'/image/profile.jpg',$content);
            }
            //---------------------------
            $data = [
                'no_story' =>true,
                'source' =>$facebook->fileToUpload(Yii::app()->params['webroot'].'/image/profile.jpg'),
                'makeprofile' =>true,

            ];

            try {
                $facebook->post('/'.Yii::app()->params['facebook']['page_id'].'/picture', $data, $PAGE_TOKEN);
                $profile->is_posted= 1;
                $profile->command= false;
                if(!$profile->save())
                    $this->send_email($profile,'error on facebook profile photo');

            } catch(Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                $profile->is_posted= 2;
                $profile->command= false;
                $this->send_email($profile,'error on facebook Graph profile photo');
                $profile->save();

            } catch(Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                $profile->is_posted= 2;
                $profile->command= false;
                $this->send_email($profile,'error on facebook SDK profile photo');
                $profile->save();
            }
        }

        return false;
    }

    private function Twitter($profile){

         if(!empty($profile)){

             $obj = $this->Obj_twitter();

             $params = array(
               /*  'width' => 1500,
                 'height' => 500,*/
                 'image' => base64_encode(file_get_contents($profile->media_url)),
             );

             $reply = $obj->account_updateProfileImage($params);

             $go =  isset($reply->errors);

             if($go){
                 $profile->is_posted= 2;
                 $profile->command= false;
                 if(!$profile->save())
                     $this->send_email($profile,'error on twitter profile photo');

                 return false;
             }

             $profile->is_posted= 1;
             $profile->command= false;

             if(!$profile->save())
                 $this->send_email($profile,'error on twitter profile photo');
             return true;

         }

        return false;

    }
    private function GetPhotoFacebook(){

        return ProfilePic::model()->get_profile_pic_facebook();
    }

    private function GetPhotoTwitter(){

        return ProfilePic::model()->get_profile_pic_twitter();
    }
}