<?php

class PageDetailsController extends FeaturesBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','test','get_most_searched','get_complete_text'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
    public function actionTest()
    {
        if(isset($_POST['predication'])){
            $items_predications = $_POST['predication'];
            $items_sources = $_POST['Sources'];
            $page_type = $_POST['pagetype'];
            $html_dom = new SimpleHTMLDOM();
            /*foreach($items_sources as $source) {*/
                $model_source = Category::model()->findByPk($items_sources);
                if ($model_source->type == 'dom') {
                    $this->TestDom($model_source,$html_dom,$page_type,$items_predications);
                }elseif($model_source->type == 'rss'){

                    $this->TestRSS($model_source,$page_type,$items_predications);
                }
            /*}*/
        }

    }
    public function TestDom($model_source,$html_dom,$page_type,$prediction){
        $counter=0;
        $max_news_count=1;
        $true_max=true;
        $model_level = CategoryLevel::model()->findAllByAttributes(array('source_id' => $model_source->id));
        $html_dom_url = $html_dom->file_get_html($model_source->url);
        foreach ($model_level as $level) {
            if(strpos($level->predication,'div[class=containermid-last] ') !== false){
                $words=explode(' ', $level->predication);
                array_shift($words);
                $level->predication =  implode(' ', $words);
                $html_content = $html_dom_url->find('div[class=containermid-last] ',0)->find($level->predication);
            }else{
                $html_content = $html_dom_url->find($level->predication);
            }
            if (isset($html_content) and !empty($html_content)) {
                $news_counter = 0;
                $success_counter=0;

                echo '<div class="col-xs-4 success_div" style="overflow: auto;"><h3 class="alert-success">success Dom prediction : <b>' . $model_source->title . '</b><span class="pull-right"><i class="fa fa-plus-square-o bigger" style="cursor:pointer"></i></span></h3>';
                foreach ($html_content as $contents) {
                    if (strpos($contents->href, 'http') === 0) {
                        $url = $contents->href;
                    } else
                        $url = Yii::app()->params['feedUrl'] . $contents->href;

                    if ($news_counter < $max_news_count) {
                        $news_counter++;
                        list($title, $description, $body_description, $video, $image, $gallery, $publishing_date, $author,$tags) = $this->get_details($url, $prediction, $page_type);
                        if ($page_type == 'title') {
                            if (!empty($title)) {
                                echo '<h5>' . $title . '</h5>';
                                $success_counter++;
                                if($true_max){
                                    $max_news_count +=10;
                                    $true_max=false;
                                }
                            } else {
                                if ($true_max)
                                    $max_news_count++;

                                echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $url . '" target="_blank" style="color:white;">' . $url . '</a></strong></b></h5>';
                            }

                        }
                        if ($page_type == 'description') {
                            if (!empty($description)) {
                                echo '<h5>' . $description . '</h5>';
                                $success_counter++;
                                if($true_max){
                                    $max_news_count +=10;
                                    $true_max=false;
                                }
                            } else {
                                if ($true_max)
                                    $max_news_count++;

                                echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $url . '" target="_blank" style="color:white;">' . $url . '</a></strong></b></h5>';
                            }
                        }
                        if ($page_type == 'body_description') {
                            if (!empty($body_description)) {
                                echo '<h5>' . $body_description . '</h5>';
                                $success_counter++;
                                if($true_max){
                                    $max_news_count +=10;
                                    $true_max=false;
                                }
                            } else {
                                if ($true_max)
                                    $max_news_count++;
                                echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $url . '" target="_blank" style="color:white;">' . $url . '</a></strong></b></h5>';
                            }
                        }
                        if ($page_type == 'video') {
                            if (!empty($video)) {
                                echo '<h5>' . $video . '</h5>';
                                $success_counter++;
                                if($true_max){
                                    $max_news_count +=10;
                                    $true_max=false;
                                }
                            } else {
                                if($true_max)
                                $max_news_count++;

                                echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $url . '" target="_blank" style="color:white;">' . $url . '</a></strong></b></h5>';
                            }
                        }
                        if ($page_type == 'image') {
                            if (!empty($image)) {
                                echo '<h5>' . $image . '</h5>';
                                $success_counter++;
                                if($true_max){
                                    $max_news_count +=10;
                                    $true_max=false;
                                }
                            } else {
                                if ($true_max)
                                    $max_news_count++;

                                echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $url . '" target="_blank" style="color:white;">' . $url . '</a></strong></b></h5>';
                            }
                        }
                        if ($page_type == 'gallery') {
                            if (!empty($gallery)) {
                                $success_counter++;
                                foreach ($gallery as $gall) {
                                    echo '<h5>' . $gall . '</h5>';

                                }
                                if($true_max){
                                    $max_news_count +=10;
                                    $true_max=false;
                                }
                            } else {
                                if ($true_max)
                                    $max_news_count++;

                                echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $url . '" target="_blank" style="color:white;">' . $url . '</a></strong></b></h5>';
                            }
                        }
                        if ($page_type == 'publishing_date') {
                            if (!empty($publishing_date)) {
                                echo '<h5>' . $publishing_date . '</h5>';
                                $success_counter++;
                                if($true_max){
                                    $max_news_count +=10;
                                    $true_max=false;
                                }
                            } else {
                                if ($true_max)
                                    $max_news_count++;

                                echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $url . '" target="_blank" style="color:white;">' . $url . '</a></strong></b></h5>';
                            }
                        }
                        if ($page_type == 'creator') {
                            if (!empty($author)) {
                                echo '<h5>' . $author . '</h5>';
                                $success_counter++;
                                if($true_max){
                                    $max_news_count +=10;
                                    $true_max=false;
                                }
                            } else {
                                if ($true_max)
                                    $max_news_count++;

                                echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $url . '" target="_blank" style="color:white;">' . $url . '</a></strong></b></h5>';
                            }
                        }
                        if ($page_type == 'all_media') {
                            if (!empty($image)) {
                                echo '<h5>' . $image . '</h5>';
                                $success_counter++;
                                if($true_max){
                                    $max_news_count +=10;
                                    $true_max=false;
                                }
                            } else {
                                if ($true_max)
                                    $max_news_count++;

                                echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $url . '" target="_blank" style="color:white;">' . $url . '</a></strong></b></h5>';
                            }
                        }
                        if ($page_type == 'Tags') {
                            if (!empty($tags)) {
                                foreach ($gallery as $gall) {
                                    echo '<h5>' . $gall . '</h5>';

                                }
                                $success_counter++;
                                if($true_max){
                                    $max_news_count +=10;
                                    $true_max=false;
                                }
                            } else {
                                if ($true_max)
                                    $max_news_count++;

                                echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $url . '" target="_blank" style="color:white;">' . $url . '</a></strong></b></h5>';
                            }
                        }

                    }
                }
                echo '<h5 style="color:darkred">Data total Success: ' .$success_counter.'/'. $news_counter . '</h5>';
                if($success_counter ==0) {
                    echo '<input type="hidden" class="hidden_value" value="' . $model_source->id . '"/>';
                }
                echo '</div>';
            } else {
                $data['invalid_urls'][$counter] = $model_source;
                $counter++;
            }
        }

        if (isset($data['invalid_urls'])) {
            echo '<div class="col-xs-4">';
            foreach ($data['invalid_urls'] as $invlid_urls) {
                echo '<h3 class="alert-danger" data-value="' . $invlid_urls->id . '">Invalid Predication Dom:  <b>' . $invlid_urls->title . '</b></h3>';
            }
            echo '</div>';
        }
    }
    public function TestRSS($model_source,$page_type,$prediction){
        $counter=0;
        $rss = file_get_contents($model_source->url_rss);

        $xml = @simplexml_load_string($rss);

        if($xml){
            if(isset($xml->channel->item) and !empty($xml->channel->item)){
                $news_counter = 0;
                $success_counter=0;


                echo '<div class="col-xs-4 success_div" style="overflow: auto;"><h3 class="alert-success">success rss prediction URL: <b>' . $model_source->title . '</b><span class="pull-right"><i class="fa fa-plus-square-o bigger" style="cursor:pointer"></i></span></h3>';
                foreach($xml->channel->item as $item) {

                    if ($news_counter < 3) {
                        $news_counter++;
                        if (isset($item->link)) {

                            list($title, $description, $body_description, $video, $image, $gallery, $publishing_date, $author) = $this->get_details($item->link, $prediction, $page_type);
                            if ($page_type == 'title') {
                                if (!empty($title)) {
                                    echo '<h5>' . $title . '</h5>';
                                    $success_counter++;
                                } else
                                    echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $item->link . '" target="_blank" style="color:white;">' . $item->link . '</a></strong></b></h5>';

                            }
                            if ($page_type == 'description') {
                                if (!empty($description)) {
                                    echo '<h5>' . $description . '</h5>';
                                    $success_counter++;
                                } else
                                    echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $item->link . '" target="_blank" style="color:white;">' . $item->link . '</a></strong></b></h5>';
                            }
                            if ($page_type == 'body_description') {
                                if (!empty($body_description)) {
                                    echo '<h5>' . $body_description . '</h5>';
                                    $success_counter++;
                                } else
                                    echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $item->link . '" target="_blank" style="color:white;">' . $item->link . '</a></strong></b></h5>';
                            }
                            if ($page_type == 'video') {
                                if (!empty($video)) {
                                    echo '<h5>' . $video . '</h5>';
                                    $success_counter++;
                                } else
                                    echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $item->link . '" target="_blank" style="color:white;">' . $item->link . '</a></strong></b></h5>';
                            }
                            if ($page_type == 'image') {
                                if (!empty($image)) {
                                    echo '<h5>' . $image . '</h5>';
                                    $success_counter++;
                                } else
                                    echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $item->link . '" target="_blank" style="color:white;">' . $item->link . '</a></strong></b></h5>';
                            }
                            if ($page_type == 'gallery') {
                                if (!empty($gallery)) {
                                    $success_counter++;
                                    foreach ($gallery as $gall) {
                                        echo '<h5>' . $gall . '</h5>';

                                    }
                                } else
                                    echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $item->link . '" target="_blank" style="color:white;">' . $item->link . '</a></strong></b></h5>';
                            }
                            if ($page_type == 'publishing_date') {
                                if (!empty($publishing_date)) {
                                    echo '<h5>' . $publishing_date . '</h5>';
                                    $success_counter++;
                                } else
                                    echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $item->link . '" target="_blank" style="color:white;">' . $item->link . '</a></strong></b></h5>';
                            }
                            if ($page_type == 'creator') {
                                if (!empty($author)) {
                                    echo '<h5>' . $author . '</h5>';
                                    $success_counter++;
                                } else
                                    echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $item->link . '" target="_blank" style="color:white;">' . $item->link . '</a></strong></b></h5>';
                            }
                            if ($page_type == 'all_media') {
                                if (!empty($image)) {
                                    echo '<h5>' . $image . '</h5>';
                                    $success_counter++;
                                } else
                                    echo '<h5 class="alert-danger">' . 'Empty<b><strong><a href="' . $item->link . '" target="_blank" style="color:white;">' . $item->link . '</a></strong></b></h5>';
                            }
                        }
                    }
                }
                echo '<h5 style="color:darkred">Data total Success: ' .$success_counter.'/'. $news_counter . '</h5>';
                if($success_counter ==0) {
                    echo '<input type="hidden" class="hidden_value" value="' . $model_source->id . '"/>';
                }
                echo '</div>';

            }
        }
        else{
            $data['invalid_urls'][$counter] = $model_source;
            $counter++;
        }

        if (isset($data['invalid_urls'])) {
            echo '<div class="col-xs-4">';
            foreach ($data['invalid_urls'] as $invlid_urls) {
                echo '<h3 class="alert-danger" data-value="' . $invlid_urls->id . '">Invalid Predication RSS:  <b>' . $invlid_urls->title . '</b></h3>';
            }
            echo '</div>';
        }
    }
    private function get_details($link,$predication,$pageType)
    {
        $html = new SimpleHTMLDOM();
        $html_url = $html->file_get_html($link);
        $author=null;
        $gallery = null;
        $publishing_date = null;
        $body_description = null;
        $description = null;
        $image = Yii::app()->params['domain']. 'image/general.jpg';;
        $video =null;
        $title=null;
        $tags=null;


            //Author
            if (isset($html_url->find($predication, 0)->content) and !empty($html_url->find($predication, 0)->content) and $pageType == 'creator') {
                $author = $html_url->find($predication, 0)->content;

            }
            elseif (isset($html_url->find($predication, 0)->plaintext) and !empty($html_url->find($predication, 0)->plaintext) and $pageType == 'creator') {
            $author = $html_url->find($predication, 0)->plaintext;
            }
            //End author

            //title
            if (isset($html_url->find($predication, 0)->plaintext) and !empty($html_url->find($predication, 0)->plaintext) and $pageType == 'title') {
                $title = $html_url->find($predication, 0)->plaintext;
            }
            elseif (isset($html_url->find($predication, 0)->content) and !empty($html_url->find($predication, 0)->content) and $pageType == 'title') {
                $title = $html_url->find($predication, 0)->content;

            }
            //End title


            //Body desc
            if (!empty($html_url->find($predication)) and $pageType == 'body_description') {
                $body_desc = $html_url->find($predication);
                if (isset($body_desc)) {
                    foreach ($body_desc as $bodies) {
                        $body_description .= $bodies->plaintext;
                    }
                }
            }

            //End body desc

            //Description
            if (isset($html_url->find($predication, 0)->content) and !empty($html_url->find($predication, 0)->content) and $pageType == 'description') {

                $description = $html_url->find($predication, 0)->content;
            }
            elseif (isset($html_url->find($predication, 0)->plaintext) and !empty($html_url->find($predication, 0)->plaintext) and $pageType == 'description') {
                $description = $html_url->find($predication, 0)->plaintext;

            }
            //End Description

            //Publishing date
            if (isset($html_url->find($predication, 0)->datetime) and !empty($html_url->find($predication, 0)->datetime) and $pageType == 'publishing_date') {
                $publishing_date = $html_url->find($predication, 0)->datetime;
            }
            if (isset($html_url->find($predication, 0)->plaintext) and !empty($html_url->find($predication, 0)->plaintext) and $pageType == 'publishing_date') {
                $publishing_date = $html_url->find($predication, 0)->plaintext;
            } elseif (isset($html_url->find($predication, 0)->content) and !empty($html_url->find($predication, 0)->content) and $pageType == 'publishing_date') {
                $publishing_date = $html_url->find($predication, 0)->content;
            }
            //End publishing date

            //All media
            if (!empty($html_url->find($predication)) and $pageType == 'all_media') {
                $script = $html_url->find($predication);
                if (!empty($script)) {
                    foreach ($script as $sc) {
                        $media = (array)json_decode($sc->innertext);
                    }
                    if (!empty($media)) {
                        if (isset($media['video'])) {
                            $video = $media['videos'];
                        }
                        if (isset($media['images'])) {
                            foreach ($media['images'] as $gall) {
                                $image = Yii::app()->params['feedUrl']. $media['images'][0];
                                if ($gall != $media['images'][0])
                                    $gallery[] = Yii::app()->params['feedUrl']. $gall;
                            }
                        }
                    }
                }
            }
            //End all media

            //image
            if (isset($html_url->find($predication, 0)->src) and !empty($html_url->find($predication, 0)->src) and $pageType == 'image') {
                if (strpos($html_url->find($predication, 0)->src, 'http') === 0) {
                    $image = $html_url->find($predication, 0)->src;

                }else
                    $image = Yii::app()->params['feedUrl'].$html_url->find($predication, 0)->src;
            } elseif (isset($html_url->find($predication, 0)->content) and !empty($html_url->find($predication, 0)->content) and $pageType == 'image') {
                if (strpos($html_url->find($predication, 0)->content, 'http') === 0)
                    $image = $html_url->find($predication, 0)->content;
                else
                    $image = Yii::app()->params['feedUrl'].$html_url->find($predication, 0)->content;

            }
            //end image

            //video
            if (isset($html_url->find($predication, 0)->src) and !empty($html_url->find($predication, 0)->src) and $pageType == 'video') {
                $video = $html_url->find($predication, 0)->src;
            } elseif (isset($html_url->find($predication, 0)->content) and !empty($html_url->find($predication, 0)->content) and $pageType == 'video') {
                $video = $html_url->find($predication, 0)->content;

            }else if (isset($html_url->find($predication, 0)->data) and !empty($html_url->find($predication, 0)->data) and $pageType == 'video') {
                $video = $html_url->find($predication, 0)->data;
            }
            //end video

            //gallery
            if (!empty($html_url->find($predication)) and $pageType == 'gallery') {
                $gall = $html_url->find($predication);
                foreach ($gall as $item) {
                    if (strpos($html_url->find($predication, 0)->src, 'http') === 0) {
                        if($image != $item->src)
                            $gallery[] =$item->src;
                    }else {
                        if ($image != Yii::app()->params['feedUrl']. $item->src)
                            $gallery[] = Yii::app()->params['feedUrl'] .$item->src;
                    }
                }

            }
            //End gallery

        //Tags
        if (!empty($html_url->find($predication)) and $pageType == 'Tags') {
            $tags_counter = $html_url->find($predication);
            foreach ($tags_counter as $item) {
                        $tags[] =$item->plaintext;
            }

        }
        //Tags
        
        return array($title,$description,$body_description,$video,$image,$gallery,$publishing_date,$author,$tags);
    }


    public function actionCreate()
	{
		$model=new PageDetails;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PageDetails']))
		{
			$model->attributes=$_POST['PageDetails'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $models=new PageDetails('search');
        $models->unsetAttributes();  // clear any default values
        if(isset($_GET['PageDetails']))
            $models->attributes=$_GET['PageDetails'];
        // Uncomment the following line if AJAX validation is needed

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['PageDetails']))
		{
			$model->attributes=$_POST['PageDetails'];
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('index',array(
			'model'=>$model,
            'models'=>$models
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model=new PageDetails;
        $models=new PageDetails('search');
        $models->unsetAttributes();  // clear any default values
        if(isset($_GET['PageDetails']))
            $models->attributes=$_GET['PageDetails'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['PageDetails']))
        {
            if(isset($_POST['submit_to_all'])){
                $_POST['PageDetails']['source_id'] =Category::model()->findAll('deleted=0');
            }

            if(isset($_POST['PageDetails']['source_id'])){

                $source = $_POST['PageDetails']['source_id'];
                foreach($source as $sources) {
                    $model->attributes = $_POST['PageDetails'];
                    $model->id=null;
                    $model->source_id = $sources->id;
                    $model->created_at = date('Y-m-d H:i:s');
                    $model->setIsNewRecord(true);
                    $model->save();
                }
            }
            $this->redirect(array('index'));
        }

        $this->render('index',array(
            'model'=>$model,
            'models'=>$models
        ));
	}

    public function actionGet_most_searched()
    {
        if(isset($_POST['page_type_id'])){
            $val =null;
            $array_count =array();
            $most_searched = PageDetails::model()->findAllByAttributes(array('page_type_id'=>$_POST['page_type_id']));
            if(!empty($most_searched)) {
                foreach ($most_searched as $search) {
                    array_push($array_count, $search->predication);
                }
                $c = array_count_values($array_count);
                $val = array_search(max($c), $c);
            }
            echo $val;
        }
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PageDetails('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PageDetails']))
			$model->attributes=$_GET['PageDetails'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PageDetails the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PageDetails::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PageDetails $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='page-details-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
