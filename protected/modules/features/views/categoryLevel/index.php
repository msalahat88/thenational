<?php
/* @var $this CategoryLevelController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Category Level',
);

?>
<script>
	$(document).ready(function(){
		$('#test').click(function(){
			$('.predict').html('');
			var sources = [];
			$.each($('#CategoryLevel_source_id option:selected'),function(key,val){
				ajax(key, val.value,$('#CategoryLevel_source_id option:selected').length);

			});
			function ajax(key,sources,all_items) {
				$.ajax({
					url: '<?PHP echo CController::createUrl('test') ?>',
					timeout:1000000,

					type:'post',
					data: {
						predication: $("#CategoryLevel_predication").val(),
						Sources: sources
					},
					cache: false,

					success: function (data) {
						$('.predict').append(data);
						$.each($('h3'),function () {
							var data_values = $(this).data("value");
							$.each($('#CategoryLevel_source_id option:selected'),function(key,val){
								if(data_values == val.value){
									$(this).attr('disabled',true);
								}
							});
						});
						$('#disabled_button').attr('disabled',false);
					}, error: function(jqXHR, textStatus){
						if(textStatus === 'timeout')
						{
							alert('Failed from timeout');
							//do something. Try again perhaps?
						}
					}
				});

			}
		})
	})
</script>
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-body">
					<?php
					$this->widget(
						'booster.widgets.TbTabs',
						array(
							'type' => 'tabs', // 'tabs' or 'pills'
							'tabs' => array(
								array(
									'label' => 'Category source',
									'buttonType'=>'url',

									'url'=>Yii::app()->baseUrl.'/category/categoryFeature'
								),
								array('label' => 'Category Level(Html dom)',
									'buttonType'=>'url',
									'active' => true,

									'url'=>Yii::app()->baseUrl.'/features/categoryLevel/index'
								),
								array('label' => 'Page details',
									'buttonType'=>'url',
									'url'=>Yii::app()->baseUrl.'/features/pageDetails/index'
								),

							),
						)
					);
					?>
					<h2>Quick create</h2>
					<?php $this->renderPartial('_form', array('model'=>$model)); ?>

					<?php
					$this->widget('booster.widgets.TbGridView', array(
						'id'=>'hashtag-grid',
						'type' => 'striped bordered condensed',
						'template' => '{pager}{items}{summary}{pager}',
						'enablePagination' => true,
						'pager' => array(
							'class' => 'booster.widgets.TbPager',
							'nextPageLabel' => 'Next',
							'prevPageLabel' => 'Previous',
							'htmlOptions' => array(
								'class' => 'pull-right'
							)
						),
						'htmlOptions' => array(
							'class' => 'table-responsive'
						),
						'dataProvider' => $models->search(),
						'filter' => $models,
						'columns' => array(
							/*	array(
                                    'name'=>'id',
                                    'visible'=>$model->visible_id?true:false,
                                ),*/
							array(
								'name' => 'source_id',
								'value' => 'isset($data->source->title)?$data->source->title:null',

							),


							array(
								'name'=>'predication',

							),
							array(
								'class' => 'booster.widgets.TbButtonColumn',
								'header' => 'Options',
								'template' => '{view}{update}{delete}',
								'buttons' => array(
									'view' => array(
										'label' => 'View',
										'icon' => 'fa fa-eye',
									),
									'update' => array(
										'label' => 'Update',
										'icon' => 'fa fa-pencil-square-o',
									),
									'delete' => array(
										'label' => 'Delete',
										'icon' => 'fa fa-times',
									),
								)
							),
						)));?>

					<div id="link-img">
						<div id="loading" style="display: none;margin:0 auto;" align="center">
							<p><img src="<?php echo Yii::app()->baseUrl . "/image/updateimg.gif" ?>" class="loading-imge"/></p>
						</div>
					</div>
					<div class="col-xs-12 predict">

					</div>
				</div>
			</div>
		</div>
</section>