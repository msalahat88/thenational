<?php
/* @var $this SubCategoriesController */
/* @var $model SubCategories */

$this->pageTitle = "Sub sections | View";

$this->breadcrumbs=array(
	'Sub sections'=>array('admin'),
	$model->title,
);
?>
<section class="content">
	<?PHP if(Yii::app()->user->hasFlash('create')){ ?>
		<div class="callout callout-success" style="margin-top:20px;">
			<h4>created successfully. </h4>
		</div>

	<?PHP } if(Yii::app()->user->hasFlash('update')){ ?>
		<div class="callout callout-info" style="margin-top:20px;">
			<h4>updated successfully. </h4>
		</div>
	<?php } ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-md-9">	<?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Action',
										'items' => array(
											array('label' => 'Update', 'url'=>array('update', 'id'=>$model->id)),
											array('label' => 'Delete', 'url' => '#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
											array('label' => 'Manage', 'url'=>array('admin'))
										)
									),
								),/*'htmlOptions'=>array(
								'class'=>'pull-right	'
							)*/
							)
						);

						?></div>
					<div class="col-md-3" style=" text-align: left;">
				 <?php echo Yii::app()->params['statement']['previousPage']; ?>





						</div>
				</div>
				<div class="box-body">
					<?php $this->widget('booster.widgets.TbDetailView', array(
						'data'=>$model,
						'attributes'=>array(


							array(
								'name'=>'title',
								'type'=>'raw',
								'value'=>CHtml::link($model->title,$model->url,array('target'=>'_blank')),
							),
							array(
								'name' => 'active',
								'value' => $model->active == 1 ? "active" : "disabled",
							),
							array(
								'name'=>'Section',
								'filter'=>CHtml::listData(Category::model()->findAll('deleted = 0 '),'id','title'),
								'value'=>$model->category->title,
							),
 							'created_at',

						),
					)); ?>
				</div>
			</div>
		</div>
		</div>
</section>

