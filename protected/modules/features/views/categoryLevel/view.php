<?php
/* @var $this CategoryLevelController */
/* @var $model CategoryLevel */

$this->breadcrumbs=array(
	'Category Levels'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CategoryLevel', 'url'=>array('index')),
	array('label'=>'Create CategoryLevel', 'url'=>array('create')),
	array('label'=>'Update CategoryLevel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CategoryLevel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CategoryLevel', 'url'=>array('admin')),
);
?>

<h1>View CategoryLevel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'source_id',
		'predication',
		'created_at',
		'updated_at',
		'created_by',
		'updated_by',
	),
)); ?>
