<?php

class ReScheduledCommand extends BaseCommand
{
    // Please to use this command , run at 7:00 am

    public function run($args){

        system("clear");

        $this->TimeZone();

        $criteria = New CDbCriteria();

        $criteria->condition = 'title= "Main Page"';

        $section = Category::model()->find($criteria);

        if(empty($section->id))
                return false;

        $times = Yii::app()->db->createCommand("
                                      SELECT
                platform_category_settings.obj_id,
                platform_category_settings.platform_id,
                time_settings.start_time,
                time_settings.end_time,
                time_settings.is_direct_push,
                time_settings.direct_push_start_time,
                time_settings.direct_push_end_time,
                time_settings.gap_time
                FROM platform_category_settings
                
                INNER JOIN days_settings
                ON days_settings.platform_category_settings_id =platform_category_settings.id
                
                INNER JOIN day
                ON days_settings.day_id =day.id
                
                INNER JOIN time_settings
                ON time_settings.platform_category_settings_id =platform_category_settings.id
                INNER JOIN settings_obj
                ON settings_obj.id =platform_category_settings.obj_id
                
                 WHERE 
                 ( platform_category_settings.category_id = " . $section->id . " or platform_category_settings.category_id IS NULL )  
                 and 
                 platform_category_settings.platform_id = 2
                 and
                 day.title = DAYNAME(NOW())
                 and 
                 settings_obj.active = 1
                 ORDER BY category_id desc;
        ")->queryAll();


        $obj = null;

        $start = null;

        $end = null;

        $gap = null;

        foreach ($times as $time) {

            $timeItem = (object)$time;

            $obj = $timeItem->obj_id;

            $gap = $timeItem->gap_time ;

            $start = strtotime(date('Y-m-d') . ' ' . $timeItem->start_time);

            $end = strtotime( date('Y-m-d') . ' ' . $timeItem->end_time )-(60*30);

            if(date('A',$start) == 'AM')
                break;

        }

        $start_obj = date('Y-m-d H:i:00',$start);

        $end_obj   = date('Y-m-d H:i:00',$end);

        $criteria = New CDbCriteria();

        $criteria->condition = ' `catgory_id` = '.$section->id.' AND `is_posted` = 0 AND `is_scheduled` = 0 AND `platform_id` = 2 AND `generated` IN ("auto", "clone") AND `reason` = "time"  and ( DATE_FORMAT(schedule_date,\'%Y-%m-%d %H:%i:%00\')  > DATE_FORMAT( DATE_SUB("'.date('Y-m-d H:i:00').'", INTERVAL 24 HOUR), \'%Y-%m-%d %H:%i:%00\' ) AND  DATE_FORMAT(schedule_date,\'%Y-%m-%d %H:%i:%00\')  <= DATE_FORMAT("'.date('Y-m-d H:i:00').'",\'%Y-%m-%d %H:%i:%00\') ) ORDER BY  `created_at` DESC';

        $postRe_count = PostQueue::model()->count($criteria);

        if($postRe_count==0)
            return false;

        $postRe = PostQueue::model()->findAll($criteria);

        $criteria->condition = ' `is_posted` = 0 AND `is_scheduled` = 1 AND `platform_id` = 2 and ( DATE_FORMAT(schedule_date,\'%Y-%m-%d %H:%i:%00\')  > DATE_FORMAT("'.$start_obj.'","%Y-%m-%d %H:%i:%00" ) AND DATE_FORMAT(schedule_date,\'%Y-%m-%d %H:%i:%00\')  <= DATE_FORMAT("'.$end_obj.'",\'%Y-%m-%d %H:%i:%00\')) ORDER BY  `schedule_date` DESC';

        $post = PostQueue::model()->find($criteria);

        if($post) {
            $post = $post->schedule_date;
        }else{
            $post = $start_obj;
        }

        foreach ($postRe as $item) {

            $schedule_date = strtotime($post . ' + ' . $gap . ' minutes');

            if($schedule_date <=$end){

                $item->main_category_id = $obj;

                $item->is_scheduled = 1;

                $item->schedule_date = date('Y-m-d H:i:00',$schedule_date);

                $item->save(false);

                $post = $item->schedule_date;

            }else{
                break;
            }

        }

        return true;

    }

}