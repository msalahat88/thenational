<?php

class DefaultController extends BaseController
{
	public function actionIndex()
	{
        $model = new PostQueue();
        if(isset($_GET['PostQueue'])){
            Yii::app()->session['total_from'] = date('Y-m-d',strtotime($_GET['PostQueue']['schedule_date']));
            Yii::app()->session['total_to'] = date('Y-m-d',strtotime($_GET['PostQueue']['to']));

        }

        if(isset(Yii::app()->session['total_from'])){
            $from = Yii::app()->session['total_from'];

        } else{
            $from = date('Y-m-d');
        }
        if(isset(Yii::app()->session['total_to'])){
            $to = Yii::app()->session['total_to'];
        }else{
            $to = date('Y-m-d');
        }

        $model->schedule_date = $from;
        $model->to = $to;

        $total_posts =  PostQueue::model()->total_post_query(date('Y-m-d 00:00:00',strtotime($from)),date('Y-m-d 23:59:59',strtotime($to)));
        $total_news_posts = News::model()->total_news_post_query(date('Y-m-d 00:00:00',strtotime($from)),date('Y-m-d 23:59:59',strtotime($to)));

        $this->render('index',array(
			/*'server_memory'=>$this->convert(memory_get_usage(true)),
			'server_cpu'=>$this->get_server_cpu_usage(),*/
			'total_news_posts'=>$total_news_posts,
			'all_news'=>News::model()->get_all_news_by_date(date('Y-m-d 00:00:00',strtotime($from)),date('Y-m-d 23:59:59',strtotime($to))),
            'total_posts'=>$total_posts,'model'=>$model
		));
	}

	function convert($size)
	{
		$unit=array('b','kb','Mb','gb','tb','pb');
		return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
	}

	function get_server_cpu_usage(){

		$load = sys_getloadavg();
		return $load[0];

	}


	public function array_sum_generated($array,$key){

	    $counter = 0;

        $array_data = array($key=>array(
            'total'=>$counter,
            'facebook'=>array(
                'total'=>0,
                'image'=>0,
                'video'=> 0,
                'text' => 0,
                'preview' => 0,
            ),
            'twitter'=>array(
                'total'=>0,
                'image'=>0,
                'video'=> 0,
                'text' => 0,
                'preview' => 0,
            ),
            'instagram'=>array(
                'total'=>0,
                'image'=>0,
                'video'=> 0,
                'text' => 0,
                'preview' => 0,
            ),
        ));

        foreach ($array as $item)
            if($item['generated']==$key){

                $array_data[$key]['total'] +=$item['countedid'];

                if($item['title'] == 'Facebook'){
                    $array_data[$key]['facebook']['total']+=$item['countedid'];
                    if($item['type'] == 'Preview')
                        $array_data[$key]['facebook']['preview']+=$item['countedid'];
                    if($item['type'] == 'Image')
                        $array_data[$key]['facebook']['image']+=$item['countedid'];
                    if($item['type'] == 'Text')
                        $array_data[$key]['facebook']['text']+=$item['countedid'];
                    if($item['type'] == 'Video')
                        $array_data[$key]['facebook']['video']+=$item['countedid'];
                }

                if($item['title'] == 'Twitter') {
                    $array_data[$key]['twitter']['total'] += $item['countedid'];
                    if($item['type'] == 'Preview')
                        $array_data[$key]['twitter']['preview']+=$item['countedid'];
                    if($item['type'] == 'Image')
                        $array_data[$key]['twitter']['image']+=$item['countedid'];
                    if($item['type'] == 'Text')
                        $array_data[$key]['twitter']['text']+=$item['countedid'];
                    if($item['type'] == 'Video')
                        $array_data[$key]['twitter']['video']+=$item['countedid'];
                }

                if($item['title'] == 'Instagram'){
                    $array_data[$key]['instagram']['total'] += $item['countedid'];
                    if($item['type'] == 'Preview')
                        $array_data[$key]['instagram']['preview']+=$item['countedid'];
                    if($item['type'] == 'Image')
                        $array_data[$key]['instagram']['image']+=$item['countedid'];
                    if($item['type'] == 'Text')
                        $array_data[$key]['instagram']['text']+=$item['countedid'];
                    if($item['type'] == 'Video')
                        $array_data[$key]['instagram']['video']+=$item['countedid'];
                }
            }


        return  $array_data;
    }

}