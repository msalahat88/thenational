<?php

/**
 * CustomSettingsForm class.
 * CustomSettingsForm is the data structure for keeping
 */
class CustomSettingsForm extends CFormModel
{
	public $platform_ids;

    public $category_ids;

    public $day_ids;

    public $start_time;

    public $end_time;

    public $is_direct_push;

    public $direct_push_start_time;

    public $direct_push_end_time;

    public $gap_time;

    public $name_group;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('group_name,platform_ids,start_time,end_time,gap_time,day_ids', 'required'),
			// rememberMe needs to be a boolean
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'platform_ids'=>'Platform\'s',
			'name_group'=>'Group Name',
			'category_ids'=>'categories',
			'day_ids'=>'Day\'s',
			'direct_push_end_time'=>'Direct Push end',
			'is_direct_push'=>'',
			'direct_push_start_time'=>'Direct Push start',
			'gap_time'=>'Gap Time',
		);
	}
}
