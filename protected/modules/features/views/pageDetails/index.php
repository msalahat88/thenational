<?php
/* @var $this PageDetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Page details',
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#page-details-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<script>
	$(document).ready(function(){
		$('#PageDetails_page_type_id').change(function(){
			$.post('<?PHP echo CController::createUrl('get_most_searched') ?>',{page_type_id:$('#PageDetails_page_type_id option:selected').val()},function(data){
				$('#PageDetails_predication').val(data);
			})
		});
		$('#test').click(function() {
			$('.predict').html('');
			var source = [];
			$.each($('#PageDetails_source_id option:selected'), function (key, val) {
				ajax(key, val.value,$('#PageDetails_source_id option:selected').length);

			});
			function ajax(key,sources,all_items) {
				$.ajax({
					url: '<?PHP echo CController::createUrl('test') ?>',
					timeout:100000000,

					type:'post',
					data: {
						predication: $("#PageDetails_predication").val(),
						Sources: sources,
						pagetype: $('#PageDetails_page_type_id option:selected').text()
					},
					cache: false,

					success: function (data) {
						$('.predict').append(data);
						$.each($('h3'), function () {
							var data_values = $(this).data("value");
							$.each($('#PageDetails_source_id option:selected'), function (key, val) {

								/*if (data_values == val.value) {
									$(this).attr('disabled', true);
								}*/
							});
						});
						$.each($('.hidden_value'), function () {
							var data_hidden_values = $(this).val();
							$.each($('#PageDetails_source_id option:selected'), function (key, val) {

								/*if (data_hidden_values == val.value) {
									$(this).attr('disabled', true);
								}*/
							});
						});
						$('.bigger').click(function () {
							if ($(this).hasClass('fa-plus-square-o')) {
								$(this).removeClass('fa-plus-square-o');
								$(this).addClass('fa-minus-square');
								$(this).closest('.success_div').removeClass('col-xs-4');
								$(this).closest('.success_div').addClass('col-xs-12');
							} else if ($(this).hasClass('fa-minus-square')) {
								$(this).addClass('fa-plus-square-o');
								$(this).removeClass('fa-minus-square');
								$(this).closest('.success_div').addClass('col-xs-4');
								$(this).closest('.success_div').removeClass('col-xs-12');
							}
						});
						$('#disabled_button').attr('disabled', false);
						$("#loadingimg").hide();
					}, error: function(jqXHR, textStatus){
						if(textStatus === 'timeout')
						{
							alert('Failed from timeout');
							//do something. Try again perhaps?
						}else{
							alert("try again");
						}
					}
				});

			}
		})
	})
</script>
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">

				<div class="box-body">
					<?php
					$this->widget(
						'booster.widgets.TbTabs',
						array(
							'type' => 'tabs', // 'tabs' or 'pills'
							'tabs' => array(
								array(
									'label' => 'Category source',
									'buttonType'=>'url',

									'url'=>Yii::app()->baseUrl.'/category/categoryFeature'
								),
								array('label' => 'Category Level(Html dom)',
									'buttonType'=>'url',
									'url'=>Yii::app()->baseUrl.'/features/categoryLevel/index'
								),
								array('label' => 'Page details',
									'buttonType'=>'url',
									'active' => true,
									'url'=>Yii::app()->baseUrl.'/features/pageDetails/index'
								),

							),
						)
					);
					?>
					<h2>Quick create</h2>
					<?php $this->renderPartial('_form', array('model'=>$model)); ?>

					<?php
					$this->widget('booster.widgets.TbGridView', array(
						'id'=>'page-details-grid',
						'type' => 'striped bordered condensed',
						'template' => '{pager}{items}{summary}{pager}',
						'enablePagination' => true,
						'pager' => array(
							'class' => 'booster.widgets.TbPager',
							'nextPageLabel' => 'Next',
							'prevPageLabel' => 'Previous',
							'htmlOptions' => array(
								'class' => 'pull-right'
							)
						),
						'htmlOptions' => array(
							'class' => 'table-responsive'
						),
						'dataProvider' => $models->search(),
						'filter' => $models,
						'columns' => array(

							array(
								'name' => 'source_id',
								'value' => 'isset($data->source->title)?$data->source->title:null',
								'filter'=>CHtml::listData(Category::model()->findAllByAttributes(array('deleted'=>0)),'id','title'),
							),
							array(
								'name'=>'page_type_id',
								'value'=>'$data->pageType->title',
								'filter'=>CHtml::listData(PageTypes::model()->findAll(),'id','title'),


							),
							array(
								'name'=>'predication',
							),
							array(
								'class' => 'booster.widgets.TbButtonColumn',
								'header' => 'Options',
								'template' => '{view}{update}{delete}',
								'buttons' => array(
									'view' => array(
										'label' => 'View',
										'icon' => 'fa fa-eye',
									),
									'update' => array(
										'label' => 'Update',
										'icon' => 'fa fa-pencil-square-o',
									),
									'delete' => array(
										'label' => 'Delete',
										'icon' => 'fa fa-times',
									),
								)
							),
						)));?>
					<div id="link-img">
						<div id="loading" style="display: none;margin:0 auto;" align="center">
							<p><img src="<?php echo Yii::app()->baseUrl . "/image/updateimg.gif" ?>" class="loading-imge"/></p>
						</div>
					</div>
					<div class="predict">

					</div>
				</div>
			</div>
		</div>
		</div>
</section>