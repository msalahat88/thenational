<?php
class ProfilePicCommand extends BaseCommand{


    public function run($args){
        $this->TimeZone();
        $profile = $this->GetProfilePicFacebook();
        $this->Facebook($profile);
        $profile = $this->GetProfilePicTwitter();
        $this->Twitter($profile);
    }

    private function Facebook($profile){
        if(!empty($profile)){
            list($facebook,$PAGE_TOKEN)=$this->Load();
            $post=array(
                //'source' => '@' . 'profile.jpg',
                'no_story' => true // suppress automatic image upload story, optional
            );

            if(!empty($profile->media_url)){
                $content = file_get_contents($profile->media_url);
                file_put_contents(Yii::app()->params['webroot'].'/image/profile.jpg',$content);

                /*$post['source']='@'.Yii::app()->params['webroot'].'/image/profile.jpg';*/
            }
            //---------------------------
            $data = [
                'no_story' =>true,
                'source' =>$facebook->fileToUpload(Yii::app()->params['webroot'].'/image/profile.jpg'),
            ];

            try {
                $response = $facebook->post('/'.Yii::app()->params['facebook']['page_id'].'/photos', $data, $PAGE_TOKEN);
                $graphNode = $response->getGraphNode();
                $data = [
                    'profile' => $graphNode['id'],
                    'offset_x' => 0, // optional
                    'offset_y' => 0, // optional
                    'no_feed_story' => true // suppress automatic profile image story, optional
                ];
                $response = $facebook->post('/'.Yii::app()->params['facebook']['page_id'], $data, $PAGE_TOKEN);
                print_r($response);
                $profile->is_posted= 1;
                $profile->command= false;
                if(!$profile->save())
                    $this->send_email($profile,'error on facebook profile photo');

            } catch(Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                $profile->is_posted= 2;
                $profile->command= false;
                $this->send_email($profile,'error on facebook Graph profile photo');
                $profile->save();

            } catch(Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                $profile->is_posted= 2;
                $profile->command= false;
                $this->send_email($profile,'error on facebook SDK profile photo');
                $profile->save();
            }
        }

        return false;
    }

    private function Twitter($profile){

         if(!empty($profile)){

             $obj = $this->Obj_twitter();

             $params = array(
                 'width' => 1500,
                 'height' => 500,
                 'banner' => base64_encode(file_get_contents($profile->media_url)),
             );

             $reply = $obj->account_updateProfileBanner($params);

             $go =  isset($reply->errors);

             if($go){
                 $profile->is_posted= 2;
                 $profile->command= false;
                 if(!$profile->save())
                     $this->send_email($profile,'error on twitter profile photo');

                 return false;
             }

             $profile->is_posted= 1;
             $profile->command= false;

             if(!$profile->save())
                 $this->send_email($profile,'error on twitter profile photo');
             return true;

         }

        return false;

    }
    private function GetProfilePicFacebook(){

        return ProfilePic::model()->get_profile_pic_facebook();
    }

    private function GetProfilePicTwitter(){

        return ProfilePic::model()->get_profile_pic_twitter();
    }
}