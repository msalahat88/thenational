<?php
/* @var $this PageDetailsController */
/* @var $model PageDetails */

$this->breadcrumbs=array(
	'Page Details'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PageDetails', 'url'=>array('index')),
	array('label'=>'Create PageDetails', 'url'=>array('create')),
	array('label'=>'View PageDetails', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PageDetails', 'url'=>array('admin')),
);
?>

<h1>Update PageDetails <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>