<?php

class m161220_171123_reason extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE `post_queue` ADD `reason` ENUM('time','other') NOT NULL DEFAULT 'other' AFTER `approved`;");
	}

	public function down()
	{
		echo "m161220_171123_reason does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}