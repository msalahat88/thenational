<?php
class CategoryCommand extends BaseCommand{

    public function run($args){
        $html = new SimpleHTMLDOM();
        $html_url = $html->file_get_html(Yii::app()->params['feedUrlRss']);
        $html_categories = $html_url->find('div [class=sitemapbox] ul li[class=sitemap_top]');
        foreach ($html_categories as $html_category) {
            $rss_url = Yii::app()->params['feedUrl'].$html_category->first_child()->href;
            $title = trim($html_category->first_child()->plaintext);
            $title_url = str_replace(array(' '),'',$title);
            $title_url = str_replace(array('&'),'-',$title_url);
            $url = Yii::app()->params['feedUrl'].'/'.strtolower($title_url);
            //$section = Category::model()->findByAttributes(array('url_rss'=>$rss_url));
            if($title != 'Full Site RSS' and $title != 'Blogs'){
                /*if(empty($section)){
                    $section = new Category();
                    $section->id = null;
                    $section->setIsNewRecord(true);
                    $section->title = $title;
                    $section->url = $url;
                    $section->url_rss = $rss_url;
                    $section->active = 1;
                    $section->deleted = 0;
                    if(!empty($section->url_rss))
                        $section->type ='rss';
                    else
                        $section->type ='dom';
                    $section->lang='en';
                    if(!$section->save(false))
                        print_r($section->getErrors());
                }*/
                /*if(!empty($section)){*/
                    foreach($html_category->find('ul li a') as $one_sub){

                        $rss_url = Yii::app()->params['feedUrl'].$one_sub->href;
                        $title2 = $one_sub->plaintext;
                        $url = Yii::app()->params['feedUrl'].'/'.strtolower($title).'/'.strtolower($title2);
                        $sub_section = Category::model()->findByAttributes(array('url_rss'=>$rss_url));
                        if(empty($sub_section)){
                            $section = new Category();
                            $section->id = null;
                            $section->setIsNewRecord(true);
                            $section->title = $title2;
                            $section->url = $url;
                            $section->url_rss = $rss_url;
                            $section->active = 1;
                            $section->deleted = 0;
                            if(!empty($section->url_rss))
                                $section->type ='rss';
                            else
                                $section->type ='dom';
                            $section->lang='en';
                            if(!$section->save(false))
                                print_r($section->getErrors());
                        }
                    }
                /*}*/
            }
        }
    }
}