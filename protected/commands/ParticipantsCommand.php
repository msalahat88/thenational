<?php
class ParticipantsCommand extends BaseCommand{

    private $parent = null;
    public function run($args){
        $this->TimeZone();
        $url = 'http://www.emaratalyoum.com/';

        if(empty($args)){
            $this->most_read($url);
            $this->most_comment($url);
        }else{
            if($args[0]=='comment'){
                $this->most_comment($url);
            }else{
                $this->most_read($url);
            }
        }

    }

    private function clear_text($text){
        return str_replace(array('&8221','&8220'),'',$text);
    }

    private function most_comment($url){
        $this->parent = null;
        $html = new SimpleHTMLDOM();

        $hts = $html->file_get_html($url);

        $shortUrl = new ShortUrl();

        $most_readed = $hts->find('div[class=tabsholder]',-1);

        $most_readed= $most_readed->find(' ul li a');

        $count = 0;

        $read = 'الأكثر تعليقا على #الامارات_اليوم';
        $read_twitter = $read;

        $read .=PHP_EOL;
        $read_twitter .=PHP_EOL;

        foreach($most_readed as $item){
            $read .=$this->clear_text($item->innertext).PHP_EOL.$shortUrl->short(urldecode('http://www.emaratalyoum.com'.$item->href)).PHP_EOL;
            $count++;
            if($count < 3)
                $read_twitter .=$shortUrl->short(urldecode('http://www.emaratalyoum.com'.$item->href)).PHP_EOL;
            if( $count == 3 )
                break;
        }
        $read_twitter .=PHP_EOL;
        $read .=PHP_EOL;
        $read .= 'شاركنا بتعليق';
        $read_twitter .= 'شاركنا بتعليق';

        $image = Yii::app()->params['domain'].'image/comment.png';

        $platform = Platform::model()->findAll('deleted = 0 ');

        foreach ($platform as $item) {
            if($item->title == 'Facebook')
                $this->add_post_queue($read,date('Y-m-d').' 08:00:00',$image,$item->id);

            if($item->title == 'Twitter')
                $this->add_post_queue($read_twitter,date('Y-m-d').' 08:00:00',$image,$item->id);
        }



    }

    private function most_read($url){

        $this->parent = null;
        $html = new SimpleHTMLDOM();

        $hts = $html->file_get_html($url);

        $shortUrl = new ShortUrl();

        $most_commented = $hts->find('div[class=tabsholder]',0);

        $most_commented = $most_commented->find(' ul li a');

        $comment = 'الأكثر قراءة على #الامارات_االيوم';
        $comment_twitter = 'الأكثر قراءة على #الامارات_االيوم';

        $comment .=PHP_EOL;
        $comment_twitter .=PHP_EOL;

        $count=0;

        foreach($most_commented as $item){
            $comment .=$this->clear_text($item->innertext).PHP_EOL.$shortUrl->short(urldecode('http://www.emaratalyoum.com'.$item->href)).PHP_EOL;
            $comment_twitter .=$shortUrl->short(urldecode('http://www.emaratalyoum.com'.$item->href)).PHP_EOL;
            $count++;
            if( $count == 3 )
                break;
        }
        $image = Yii::app()->params['domain'].'image/read.png';
        $platform = Platform::model()->findAll('deleted = 0');
        foreach ($platform as $item) {
            if($item->title == 'Facebook')
                $this->add_post_queue($comment,date('Y-m-d').' 17:00:00',$image,$item->id);

            if($item->title == 'Twitter')
                $this->add_post_queue($comment_twitter,date('Y-m-d').' 17:00:00',$image,$item->id);
        }
    }


    private function add_post_queue($text,$dateTime,$image,$platform){

        $PostQueue = new PostQueue();
        $PostQueue->setIsNewRecord(true);
        $PostQueue->id= null;
        $PostQueue->command= false;
        $PostQueue->type = 'Image';
        $PostQueue->post = $text;
        $PostQueue->schedule_date = $dateTime;
        $PostQueue->catgory_id = null;
        $PostQueue->media_url = $image;
        $PostQueue->link = null;
        $PostQueue->is_posted = 0;
        $PostQueue->news_id =null;
        $PostQueue->post_id =null;
        $PostQueue->parent_id =$this->parent;
        $PostQueue->is_scheduled =1;
        $PostQueue->platform_id =$platform;
        $PostQueue->generated ='manual';
        $PostQueue->created_at =date('Y-m-d H:i:s');
        if(!$PostQueue->save())
            $this->send_email($PostQueue,'error on participants');


        if($this->parent == null)
            $this->parent = $PostQueue->id;

    }
}